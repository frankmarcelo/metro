<?php

class TopFiftyController extends AppController {
    
    public $components = array(
        //'DebugKit.Toolbar',
        'ImageResize'
    );
    
    public $name = 'TopFifty';
    public $uses = array('TopRestaurants');
    public $helpers = array(
        'TidyFilter'
    );
    
    public $paginate = array(
        'TopRestaurants' => array(
            'conditions' => array('TopRestaurants.status'=> 1),
            'limit'	 => 20,
            'page'	 => 1,
            'order'	 => array('TopRestaurants.title' => 'ASC')
        )
    );
    
    private $options = array();
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->options = array(
            'script_url' => $_SERVER['PHP_SELF'],
            'upload_destination_dir' => dirname(dirname(dirname(__FILE__))).'/app/webroot/uploads/original/top_restaurants/',
            'upload_dir' => dirname(dirname(dirname(__FILE__))).'/app/webroot/uploads/original/top_restaurants/raw/',
            'upload_url' => dirname(dirname(dirname(__FILE__))).'/app/webroot/uploads/original/top_restaurants/raw/',
            'param_name' => 'files',
            'image_width' => 640,
            'image_height' => 960,
            'max_file_size' => null,
            'min_file_size' => 1,
            'accept_file_types' => '/(\.|\/)(png|gif|jpg|jpeg)$/i',
            'max_number_of_files' => null,
            'discard_aborted_uploads' => true,
            'image_versions' => array(
                'thumbnail' => array(
                    'upload_dir' => dirname(dirname(dirname(__FILE__))).'/app/webroot/uploads/thumbs/480_320/top_restaurants/',
                    'upload_url' => dirname(dirname(dirname(__FILE__))).'/app/webroot/uploads/thumbs/480_320/top_restaurants/',
                    'max_width' => 320,
                    'max_height' => 480
                )
            )
        );
        Configure::write('debug',0);
    }

    public function index() {
        $this->paginate['TopRestaurants'] = array(
            'conditions'=>  array('TopRestaurants.status'=> Configure::read('status_live')),
            'order' => $this->paginate['TopRestaurants']['order']
        );
        $this->set('toprestaurants',$this->paginate('TopRestaurants'));
    }
    
    public function search(){
        //echo 'went here';
    }
    
    public function add(){
        if( $this->RequestHandler->isPost() ){
            if( $this->validateFormPost() ){
                if( $file = $this->validateFormImage() ){
                    if( !isset($file->message) ){
                        $this->TopRestaurants->begin(); 
                        $this->data['TopRestaurants']['image'] = $file->name;
                        $this->data['TopRestaurants']['status'] = Configure::read('status_live');
                        $this->data['TopRestaurants']['created_at'] = date("Y-m-d H:i:s",strtotime("now"));
                        $this->data['TopRestaurants']['updated_at'] = date("Y-m-d H:i:s",strtotime("now"));
                        if ($this->TopRestaurants->save($this->data)) {
                            $this->TopRestaurants->commit();
                            $id = $this->TopRestaurants->getLastInsertID();
                            $conditions = array(
                                'conditions' => array('TopRestaurants.status' => Configure::read("status_live") ),   
                                'fields' => array(
                                    'TopRestaurants.id',
                                    'TopRestaurants.title',
                                    'TopRestaurants.address',
                                    'TopRestaurants.phone',
                                    'TopRestaurants.details',
                                    'TopRestaurants.image',
                                    'TopRestaurants.quality_rating',
                                    'TopRestaurants.price_rating',
                                    'TopRestaurants.favourite_dish',
                                    'TopRestaurants.opening_hours',
                                    'TopRestaurants.latitude',
                                    'TopRestaurants.longitude',
                                    'TopRestaurants.facebook_id'
                                ),
                                'order' => array('TopRestaurants.title ASC')
                            );
                            $toprestaurants = $this->TopRestaurants->find('all',$conditions);
                            Cache::write('toprestaurants', $toprestaurants);
                            $this->Session->setFlash(__('You have successfully added Top 50 restaurant.', true), 'default',array('class' => 'alert alert-success'));
                            $this->redirect(array('controller' => 'top_fifty', 'action' => 'edit', $id. DIRECTORY_SEPARATOR . convertToSeoUri(strtolower(trim($this->data['TopRestaurants']['title'])))));
                        }
                    }else{
                        $this->set('toprestaurant',$this->data);
                        $this->Session->setFlash(__($file->message, true), 'default', array('class' => 'alert alert-error'));
                    }
                }else{
                    
                }
            }else{
                $this->set('toprestaurant',$this->data);
                $this->Session->setFlash(__('One of the required fields is empty.', true), 'default', array('class' => 'alert alert-error'));
            }
        }
    }
    
    public function batch_delete(){
        Configure::write('debug',0);
        $this->layout = false;
        if( $this->RequestHandler->isPost() && isset($this->data['TopRestaurants']['id']) && sizeof($this->data['TopRestaurants']['id']) > 0 ){
            $conditions = array(
                'conditions' => array(
                    'TopRestaurants.id IN ('.implode(",",$this->data['TopRestaurants']['id']).')',
                    'TopRestaurants.status' => Configure::read('status_live')
                ),
                'fields' => array('TopRestaurants.id')
            );
            $restaurants = $this->TopRestaurants->find('all',$conditions);
            if( $restaurants && sizeof($restaurants)>0 && is_array($restaurants) ){
                foreach( $restaurants as $restaurant ){
                    $this->TopRestaurants->begin(); 
                    $this->data['TopRestaurants']['id'] = intval($restaurant['TopRestaurants']['id']);
                    $this->data['TopRestaurants']['status'] = Configure::read('status_deleted');
                    $this->data['TopRestaurants']['updated_at'] = date("Y-m-d H:i:s",strtotime("now"));
                    if ($this->TopRestaurants->save($this->data)) {
                        $this->TopRestaurants->commit();
                    }
                }
                
                $conditions = array(
                    'conditions' => array('TopRestaurants.status' => Configure::read("status_live") ),   
                    'fields' => array(
                        'TopRestaurants.id',
                        'TopRestaurants.title',
                        'TopRestaurants.address',
                        'TopRestaurants.phone',
                        'TopRestaurants.details',
                        'TopRestaurants.image',
                        'TopRestaurants.quality_rating',
                        'TopRestaurants.price_rating',
                        'TopRestaurants.favourite_dish',
                        'TopRestaurants.opening_hours',
                        'TopRestaurants.latitude',
                        'TopRestaurants.longitude',
                        'TopRestaurants.facebook_id'
                    ),
                    'order' => array('TopRestaurants.title ASC')
                );
                $toprestaurants = $this->TopRestaurants->find('all',$conditions);
                Cache::write('toprestaurants', $toprestaurants);
                $this->Session->setFlash(__('Delete successful.', true), 'default',array('class' => 'alert alert-success'));
                $this->redirect(array("controller"=>"/"));
                die;
            }
        }
        $this->Session->setFlash(__('Invalid request please try again.', true), 'default', array('class' => 'alert alert-error'));
        $this->redirect(array("controller"=>"/"));
        die;
    }
    
    public function delete($id){
        Configure::write('debug',0);
        $this->layout = false;
        $toprestaurant = $this->TopRestaurants->findById($id);
        if( intval($toprestaurant['TopRestaurants']['status']) == Configure::read('status_live') ){
            $this->TopRestaurants->begin(); 
            $this->data['TopRestaurants']['id'] = $id;
            $this->data['TopRestaurants']['status'] = Configure::read('status_deleted');
            $this->data['TopRestaurants']['updated_at'] = date("Y-m-d H:i:s",strtotime("now"));
            if ($this->TopRestaurants->save($this->data)) {
                $this->TopRestaurants->commit();
                $conditions = array(
                    'conditions' => array('TopRestaurants.status' => Configure::read("status_live") ),   
                    'fields' => array(
                        'TopRestaurants.id',
                        'TopRestaurants.title',
                        'TopRestaurants.address',
                        'TopRestaurants.phone',
                        'TopRestaurants.details',
                        'TopRestaurants.image',
                        'TopRestaurants.quality_rating',
                        'TopRestaurants.price_rating',
                        'TopRestaurants.favourite_dish',
                        'TopRestaurants.opening_hours',
                        'TopRestaurants.latitude',
                        'TopRestaurants.longitude',
                        'TopRestaurants.facebook_id'
                    ),
                    'order' => array('TopRestaurants.title ASC')
                );
                $toprestaurants = $this->TopRestaurants->find('all',$conditions);
                Cache::write('toprestaurants', $toprestaurants);
                $this->Session->setFlash(__('You have successfully deleted "'.$toprestaurant['TopRestaurants']['title'].'"', true), 'default',array('class' => 'alert alert-success'));
            }else{
                $this->Session->setFlash(__('Invalid delete request', true), 'default', array('class' => 'alert alert-error'));
            }
        }else{
            $this->Session->setFlash(__('Invalid delete request', true), 'default', array('class' => 'alert alert-error'));
        }
        $this->redirect(array("controller"=>"/"));
        die;
    }
    
    public function edit_geocode(){
        Configure::write('debug',0);
        $this->layout = false;
        $id = intval($this->params['url']['id']);
        $toprestaurant = $this->TopRestaurants->findById($id);
        if( intval($toprestaurant['TopRestaurants']['status']) == Configure::read('status_live') ){
            $this->TopRestaurants->begin(); 
            $this->data['TopRestaurants']['id'] = $id;
            $this->data['TopRestaurants']['latitude'] = trim($this->params['url']['latitude']);
            $this->data['TopRestaurants']['longitude'] = trim($this->params['url']['longitude']);
            $this->data['TopRestaurants']['updated_at'] = date("Y-m-d H:i:s",strtotime("now"));
            if ($this->TopRestaurants->save($this->data)) {
                $this->TopRestaurants->commit();
                $conditions = array(
                    'conditions' => array('TopRestaurants.status' => Configure::read("status_live") ),   
                    'fields' => array(
                        'TopRestaurants.id',
                        'TopRestaurants.title',
                        'TopRestaurants.address',
                        'TopRestaurants.phone',
                        'TopRestaurants.details',
                        'TopRestaurants.image',
                        'TopRestaurants.quality_rating',
                        'TopRestaurants.price_rating',
                        'TopRestaurants.favourite_dish',
                        'TopRestaurants.opening_hours',
                        'TopRestaurants.latitude',
                        'TopRestaurants.longitude',
                        'TopRestaurants.facebook_id'
                    ),
                    'order' => array('TopRestaurants.title ASC')
                );
                $toprestaurants = $this->TopRestaurants->find('all',$conditions);
                Cache::write('toprestaurants', $toprestaurants);
                echo json_encode(true);
            }
        }
        die;
    }
    
    public function edit($id){
        $toprestaurant = $this->TopRestaurants->findById($id);
        if( intval($toprestaurant['TopRestaurants']['status']) == Configure::read('status_live') ){
            if( $this->RequestHandler->isPost() || $this->RequestHandler->isPut() ){
                if( $this->validateFormPost() ){
                    $this->TopRestaurants->begin(); 
                    
                     $imagenotification = null;
                    if( isset($this->data['TopRestaurants']['image']['name']) && 
                        !empty($this->data['TopRestaurants']['image']['name']) &&
                        intval($this->data['TopRestaurants']['image']['error']) < 1    
                    ){
                        if( $file = $this->validateFormImage() ){
                            if( !isset($file->message) ){    
                                $this->data['TopRestaurants']['image'] = $file->name;
                            }else{
                                unset($this->data['TopRestaurants']['image']);
                                $imagenotification = $file->message;
                            }
                        }
                    }else{
                        unset($this->data['TopRestaurants']['image']);
                    }
                    
                    if( is_null($imagenotification) ){
                        $this->data['TopRestaurants']['status'] = Configure::read('status_live');
                        $this->data['TopRestaurants']['updated_at'] = date("Y-m-d H:i:s",strtotime("now"));
                        if ($this->TopRestaurants->save($this->data)) {
                            $this->TopRestaurants->commit();
                            $id = intval($this->data['TopRestaurants']['id']);
                            $conditions = array(
                                'conditions' => array('TopRestaurants.status' => Configure::read("status_live") ),   
                                'fields' => array(
                                    'TopRestaurants.id',
                                    'TopRestaurants.title',
                                    'TopRestaurants.address',
                                    'TopRestaurants.phone',
                                    'TopRestaurants.details',
                                    'TopRestaurants.image',
                                    'TopRestaurants.quality_rating',
                                    'TopRestaurants.price_rating',
                                    'TopRestaurants.favourite_dish',
                                    'TopRestaurants.opening_hours',
                                    'TopRestaurants.latitude',
                                    'TopRestaurants.longitude',
                                    'TopRestaurants.facebook_id'
                                ),
                                'order' => array('TopRestaurants.title ASC')
                            );
                            $toprestaurants = $this->TopRestaurants->find('all',$conditions);
                            Cache::write('toprestaurants', $toprestaurants);
                            $this->Session->setFlash(__('You have successfully edited '.$this->data['TopRestaurants']['title'], true), 'default',array('class' => 'alert alert-success'));
                            $this->redirect(array('controller' => 'top_fifty', 'action' => 'edit', $id. DIRECTORY_SEPARATOR . convertToSeoUri(strtolower(trim($this->data['TopRestaurants']['title'])))));
                        }
                    }else{
                        $this->set('post_toprestaurant',$this->data);
                        $this->Session->setFlash(__($imagenotification, true), 'default', array('class' => 'alert alert-error'));
                    }
                }else{
                    $this->Session->setFlash(__('One of the required fields is empty.', true), 'default', array('class' => 'alert alert-error'));
                }
                $this->set('post_toprestaurant', $this->data);
            }
            $this->set('toprestaurant', $toprestaurant);
            $this->set('title_for_layout','Edit Top 50 '.ucwords(strtolower($toprestaurant['TopRestaurants']['title'])));
        }else{
            
            $this->Session->setFlash(__('Invalid request', true), 'default', array('class' => 'alert alert-error'));
            $this->redirect('/');
        }
    }
    
    private function validateFormPost(){
        if( (strlen(trim($this->data['TopRestaurants']['title']))     > 0) &&
            (strlen(trim($this->data['TopRestaurants']['address']))   > 0) &&
            (strlen(trim($this->data['TopRestaurants']['phone']))     > 0) &&
            (strlen(trim($this->data['TopRestaurants']['details']))   > 0) &&
            (strlen(trim($this->data['TopRestaurants']['latitude']))  > 0) &&
            (strlen(trim($this->data['TopRestaurants']['longitude'])) > 0)     
        ){
            return true;
        }else{
            return false;
        }
    }
    
    private function validateFormImage($validateWithImage=true){
        $notification = new stdClass();
        if( intval($this->data['TopRestaurants']['image']['error']) < 1 ){
            list($width, $height, $type, $attr) = @getimagesize($this->data['TopRestaurants']['image']['tmp_name']);
            if( intval($width) == intval($this->options['image_width']) && intval($height) == intval($this->options['image_height']) ){
                $file_uploaded = $this->handle_file_upload(
                    $this->data['TopRestaurants']['image']['tmp_name'],
                    $this->data['TopRestaurants']['image']['name'],
                    $this->data['TopRestaurants']['image']['size'],
                    $this->data['TopRestaurants']['image']['type'],
                    $this->data['TopRestaurants']['image']['error']
                );

                if( is_object($file_uploaded) && isset($file_uploaded->size) && intval($file_uploaded->size)>0 && !isset($file_uploaded->error) ){
                    $src = $this->options['upload_dir'].$file_uploaded->name;
                    $destination = $this->options['upload_destination_dir'].$file_uploaded->name;
                    $thumbs = $this->options['image_versions']['thumbnail']['upload_dir'].'thumbs_'.$file_uploaded->name;
                    $thumb_width = intval($this->options['image_versions']['thumbnail']['max_width']);
                    $thumb_height = intval($this->options['image_versions']['thumbnail']['max_height']);
                    $errors = $this->ImageResize->resize($src, $destination, $dim_setting=RESIZE_BOTH, $width = $this->options['image_width'], $this->options['image_height']);//big
                    if( !$errors ){
                        $errors = $this->ImageResize->resize($src, $thumbs, $dim_setting=RESIZE_BOTH, $width = $thumb_width, $thumb_height);//thumbnail 
                        $notification = $file_uploaded;
                    }else{
                        $notification->message = $errors;
                    }
                }else{

                    $notification->message = $file_uploaded->error;
                }

            }else{
                $notification->message = 'Image dimensions '.$width.' x '.$height.' does not comply the standard size of '.$this->options['image_width'].' x '.$this->options['image_height'];
            }
        }else{
            $notification->message = 'Please upload valid image file. Please try again.';
        }
        return $notification;
    }
    
    private function handle_file_upload($uploaded_file, $name, $size, $type, $error) {
        
        $file = new stdClass();
        $file->name = trim(basename(stripslashes($name)), ".\x00..\x20");
        $extension  = pathinfo($file->name, PATHINFO_EXTENSION);
        $file->name = md5(strtotime("now")).'.jpg';
        $file->size = intval($size);
        $file->type = $type;
        
        $error = $this->has_error($uploaded_file, $file, $error);
        
        if (!$error && $file->name) {
            
            $file_path = $this->options['upload_dir'].$file->name;
            $append_file = is_file($file_path) && $file->size > filesize($file_path);
            clearstatcache();
            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                if ($append_file) {
                    file_put_contents(
                        $file_path,
                        fopen($uploaded_file, 'r'),
                        FILE_APPEND
                    );
                } else {
                    move_uploaded_file($uploaded_file, $file_path);
                }
            } else {
                // Non-multipart uploads (PUT method support)
                file_put_contents(
                    $file_path,
                    fopen('php://input', 'r'),
                    $append_file ? FILE_APPEND : 0
                );
            }

            $old = umask(0);
            chmod($file_path, 0777);
            umask($old);
        } else {
            $file->error = $error;
        }
        return $file;
    }
    
    private function has_error($uploaded_file, $file, $error) {
        if ($error) {
            return $error;
        }
        if (!preg_match($this->options['accept_file_types'], $file->name)) {
            return 'acceptFileTypes';
        }
        if ($uploaded_file && is_uploaded_file($uploaded_file)) {
            $file_size = filesize($uploaded_file);
        } else {
            $file_size = $_SERVER['CONTENT_LENGTH'];
        }
        if ($this->options['max_file_size'] && (
                $file_size > $this->options['max_file_size'] ||
                $file->size > $this->options['max_file_size'])
            ) {
            return 'maxFileSize';
        }
        if ($this->options['min_file_size'] &&
            $file_size < $this->options['min_file_size']) {
            return 'minFileSize';
        }
        
        return $error;
    }
}