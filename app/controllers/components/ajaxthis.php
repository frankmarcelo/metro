<?php
/*Using sanitize library*/
uses('sanitize');
/********************/
class AjaxthisComponent extends Object
{
    var $controller = true;

    function startup(&$controller){
        //Instantiation du controller parent
        $this->controller = &$controller;
    }

    /**
    *Public : Call to controller action for initial parameters
    **/
    function initThis($ajaxCall = null,$ajaxAction = null,$ajaxParams = array()){
        if(!empty($ajaxCall)){
            if(!empty($ajaxAction)){
                if(empty($ajaxParams)){
                     if((strtolower($this->controller->params['controller'])==strtolower($ajaxCall))&&(strtolower($this->controller->params['action'])==strtolower($ajaxAction))){
                        $params = call_user_func(array(&$this->controller,$ajaxAction));
                     }
                     else{
                        $params = $this->controller->requestAction('/'.$ajaxCall.'/'.$ajaxAction.'/');
                     }
                }
                else{
                     if((strtolower($this->controller->params['controller'])==strtolower($ajaxCall))&&(strtolower($this->controller->params['action'])==strtolower($ajaxAction))){
                        $params = call_user_func(array(&$this->controller,$ajaxAction),$ajaxParams);
                     }
                     else{
                        $params = $this->controller->requestAction('/'.$ajaxCall.'/'.$ajaxAction.'/'.base64_encode(http_build_query($ajaxParams, '', '&')));
                    }
                }
            }
            else{
                if(empty($ajaxParams)){
                     if((strtolower($this->controller->params['controller'])==strtolower($ajaxCall))&&(strtolower($this->controller->params['action'])==strtolower($ajaxAction))){
                        $params = call_user_func(array(&$this->controller,$ajaxCall),$ajaxParams);
                     }
                     else{
                        $params = $this->controller->requestAction('/'.$ajaxCall.'/');
                     }
                }
                else{
                     if((strtolower($this->controller->params['controller'])==strtolower($ajaxCall))&&(strtolower($this->controller->params['action'])==strtolower($ajaxAction))){
                        $params = call_user_func(array(&$this->controller,$ajaxCall),$ajaxParams);
                     }
                     else{
                        $params = $this->controller->requestAction('/'.$ajaxCall.'/'.base64_encode(http_build_query($ajaxParams, '', '&')));
                    }
                }
            }
        }
        if(!isset($params)){
            return null;
        }
        else{
            return $params;
        }
    }

    /**
    *Public : Processing ajax request and rendering
    **/
    function ajaxThis($ajaxCall=null,$ajaxAction=null,$ajaxViews=null,$ajaxParams=null){
        //Decoding values
        if(!empty($ajaxCall)){
            $decodedAjaxCall = base64_decode($ajaxCall);
        }
        else{
            $decodedAjaxCall = null;
        }
        if(!empty($ajaxAction)){
            $decodedAjaxAction = base64_decode($ajaxAction);
        }
        else{
            $decodedAjaxAction = null;
        }
        if(!empty($this->controller->data)){
            if(!empty($ajaxParams)){
                $decodedAjaxParams = $this->_decodeAjaxParams($ajaxParams);
                $decodedAjaxParams = $decodedAjaxParams + $this->controller->data;
                $ajaxParams = $this->encodeAjaxParams($decodedAjaxParams);
            }
        }
        else{
            $decodedAjaxParams = $this->_decodeAjaxParams($ajaxParams);
        }
        $decodedAjaxViews = $this->_decodeAjaxViews($ajaxViews);
        //Processing values
        if(!empty($decodedAjaxCall)){
            if(empty($decodedAjaxParams)){
                if(empty($decodedAjaxAction)){
                    if((!empty($decodedAjaxViews))&&(!is_array($decodedAjaxViews))){
                        if(strtolower($this->controller->name)==strtolower($decodedAjaxCall)){
                            $ajaxParams = call_user_func(array(&$this->controller, $decodedAjaxViews));
                        }
                        else{
                            //Requesting selected action (= view name) from selected controller
                            $ajaxParams = $this->controller->requestAction('/'.$decodedAjaxCall.'/'.$decodedAjaxViews.'/');
                        }
                    }
                    else{
                        if(strtolower($this->controller->name)==strtolower($decodedAjaxCall)){
                            $ajaxParams = call_user_func(array(&$this->controller,$decodedAjaxCall));
                        }
                        else{
                            //Requesting selected controller
                            $ajaxParams = $this->controller->requestAction('/'.$decodedAjaxCall.'/');
                        }
                    }
                }
                else{
                    if(strtolower($this->controller->name)==strtolower($decodedAjaxCall)){
                        $ajaxParams = call_user_func(array(&$this->controller, $decodedAjaxAction));
                    }
                    else{
                        //Requesting selected action from selected controller
                        $ajaxParams = $this->controller->requestAction('/'.$decodedAjaxCall.'/'.$decodedAjaxAction.'/');
                    }
                }
            }
            else{
                if(empty($decodedAjaxAction)){
                    if((!empty($decodedAjaxViews))&&(!is_array($decodedAjaxViews))){
                        if(strtolower($this->controller->name)==strtolower($decodedAjaxCall)){
                            $ajaxParams = call_user_func(array(&$this->controller,$decodedAjaxViews),$decodedAjaxParams);
                        }
                        else{
                            //Requesting selected action (= view name) from selected controller  with params
                            $ajaxParams = $this->controller->requestAction('/'.$decodedAjaxCall.'/'.$decodedAjaxViews.'/'.$ajaxParams);
                        }
                    }
                    else{
                        if(strtolower($this->controller->name)==strtolower($decodedAjaxCall)){
                            $ajaxParams = call_user_func(array(&$this->controller,$decodedAjaxCall),$decodedAjaxParams);
                        }
                        else{
                            //Requesting selected controller  with params
                            $ajaxParams = $this->controller->requestAction('/'.$decodedAjaxCall.'/'.$ajaxParams);
                        }
                    }
                }
                else{
                    if(strtolower($this->controller->name)==strtolower($decodedAjaxCall)){
                        $ajaxParams = call_user_func(array(&$this->controller,$decodedAjaxAction),$decodedAjaxParams);
                    }
                    else{
                        //Requesting selected action from selected controller with params
                        $ajaxParams = $this->controller->requestAction('/'.$decodedAjaxCall.'/'.$decodedAjaxAction.'/'.$ajaxParams);
                    }
                }
            }
        }
        else{
            if(empty($decodedAjaxParams)){
                if(empty($decodedAjaxAction)){
                    if((!empty($decodedAjaxViews))&&(!is_array($decodedAjaxViews))){
                        if(strtolower($this->controller->name)==strtolower($decodedAjaxViews)){
                            $ajaxParams = call_user_func(array(&$this->controller,$decodedAjaxViews));
                        }
                        else{
                            //Requesting selected action (= view name) from selected controller
                            $ajaxParams = $this->controller->requestAction('/'.$decodedAjaxViews.'/');
                        }
                    }
                }
                else{
                    if(strtolower($this->controller->name)==strtolower($decodedAjaxAction)){
                        $ajaxParams = call_user_func(array(&$this->controller,$decodedAjaxAction));
                    }
                    else{
                        //Requesting selected action from selected controller
                        $ajaxParams = $this->controller->requestAction('/'.$decodedAjaxAction.'/');
                    }
                }
            }
            else{
                if(empty($decodedAjaxAction)){
                    if((!empty($decodedAjaxViews))&&(!is_array($decodedAjaxViews))){
                        if(strtolower($this->controller->name)==strtolower($decodedAjaxViews)){
                            $ajaxParams = call_user_func(array(&$this->controller,$decodedAjaxViews),$decodedAjaxParams);
                        }
                        else{
                            //Requesting selected action (= view name) from selected controller  with params
                            $ajaxParams = $this->controller->requestAction('/'.$decodedAjaxViews.'/'.$ajaxParams);
                        }
                    }
                }
                else{
                    if(strtolower($this->controller->name)==strtolower($decodedAjaxAction)){
                        $ajaxParams = call_user_func(array(&$this->controller,$decodedAjaxAction),$decodedAjaxParams);
                    }
                    else{
                        //Requesting selected action from selected controller with params
                        $ajaxParams = $this->controller->requestAction('/'.$decodedAjaxAction.'/'.$ajaxParams);
                    }
                }
            }
        }
        //setting the view vars (name of the element to render and ajax parameters)
        $this->controller->set('ajaxViews',$decodedAjaxViews);
        $this->controller->set($decodedAjaxCall.$decodedAjaxAction,$ajaxParams);
        //rendering the view
        $this->controller->render('ajax','ajax');
    }

    function getAjaxParams($params = null){
        if(empty($params)){
            return null;
        }
        else{
            if(is_array($params)){
                return $params;
            }
            else{
                return ($this->_decodeAjaxParams($params));
            }
        }
    }

    function encodeAjaxParams($ajaxParams){
        return base64_encode(http_build_query($ajaxParams, '', '&'));
    }

    function _decodeAjaxViews($ajaxViews){
        $clean = new Sanitize();
        if(empty($ajaxViews)){
            return null;
        }
        else{
            $clean = new Sanitize();
            $ajaxViews = explode('&', base64_decode($ajaxViews));
            if(is_array($ajaxViews)){
                foreach($ajaxViews as &$views){
                    $views = $clean->cleanValue($views);//sanitize
                    if(!(strpos($views,'\\')===false)){
                        $views = stripslashes($views);
                    }
                }
                return $ajaxViews;
            }
            else{
                $ajaxViews = $clean->cleanValue($ajaxViews);//sanitize
                return array($ajaxViews => $ajaxViews);
            }
        }
    }

    function _decodeAjaxParams($ajaxParams){
        if(empty($ajaxParams)){
            return null;
        }
        else{
            //if ajaxParams are given
            parse_str(base64_decode($ajaxParams),$ajaxParams);
            $clean = new Sanitize();
            $clean->cleanArrayR($ajaxParams);
            return $ajaxParams;
        }
    }

    //normalize the case and sort an array values and keys
    function _normalizeArrays($arr = array()){
        if(is_array($arr)){
            foreach($arr as &$subArr)
            if(is_array($subArr)){
                foreach($subArr as &$value){
                    $value = ucwords(strtolower(trim($value)));
                }
                $subArr = array_change_key_case($subArr, CASE_UPPER);
                ksort($subArr,SORT_STRING);
            }
        }
        return $arr;
    }

    //Remove empty values from any multidimensiannal array
    function _cleanArray($p_value){
        if (is_array ($p_value)){
            if ( count ($p_value) == 0) {
                $p_value = null;
            } else {
                foreach ($p_value as $m_key => $m_value) {
                    $p_value[$m_key] = $this->_cleanArray($m_value);
                    if (empty ($p_value[$m_key])) unset ($p_value[$m_key]);
                }
            }
        } else {
            if (empty ($p_value)) {
                $p_value = null;
            }
        }
        return $p_value;
    }
}
?> 