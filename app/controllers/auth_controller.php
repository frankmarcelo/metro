<?php

App::import('Sanitize');

class AuthController extends AppController {
    
    public $name = 'Users';
    public $hasOne = array(
        'Roles' => array(
            'className'  => 'Roles',
            'foreignKey' => 'Roles.id', 
            'dependent'  => true
        )
    ); 
    
    public $paginate = array(
    	'limit' => 100, 
        'order' => array('User.id' => 'desc')
    );
    
    public $helpers = array(
        'TidyFilter'
    );
	
    public $components = array(
        'Auth' ,
        'Email'
    );
/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
    public $uses = array('User');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Security->validatePost = false;
        $this->Auth->authenticate = ClassRegistry::init('User');
	$this->Auth->userScope = array('User.status' => Configure::read("status_live"));
	$this->Auth->fields = array( 'username' => 'username','password' => 'password' );
        $this->Auth->loginError = "That's not the right password!";
        $this->Auth->authError  = "Please enter your login credentials";
        
        if (in_array($this->params['action'], array('login'))) {
            $field = $this->Auth->fields['username'];
            if (!empty($this->data) && empty($this->data['User'][$field])) {
                $this->redirect(array( 'controller'=> '/'));
            }
        }
    }

    public function beforeRender() {
        parent::beforeRender();

        if (in_array($this->params['action'], array('admin_login', 'login'))) {
            if (!empty($this->data)) {
                $field = $this->Auth->fields['username'];
                $cacheName = 'auth_failed_' . $this->data['User'][$field];
                $cacheValue = Cache::read($cacheName, 'users_login');
                Cache::write($cacheName, (int)$cacheValue + 1, 'users_login');
            }
        }
    }
    
    public function admin_login() {
        $sessionMessage = $this->Session->read('Message.auth.message');
        if( isset($sessionMessage) && !empty($sessionMessage) ){
            $sessionFlash = $sessionMessage;
            $this->Session->delete('Message.auth');
            $this->Session->delete('Auth.redirect');
            $this->set( compact('sessionFlash') );
        }
	
	if( $this->Session->read('Auth.User.id') > 0 ){
            $this->redirect(array('controller'=>'/'));	
	}
    	$this->set('title_for_layout', __('Login', true));
    }
    
    public function login() {
        $sessionMessage = $this->Session->read('Message.auth.message');
        if( isset($sessionMessage) && !empty($sessionMessage) ){
            $sessionFlash = $sessionMessage;
            $this->Session->delete('Message.auth');
            $this->Session->delete('Auth.redirect');
            $this->set( compact('sessionFlash') );
        }
	
	if( $this->Session->read('Auth.User.id') > 0 ){
            $this->redirect(array('controller'=>'/'));	
	}
    	$this->set('title_for_layout', __('Login', true));
    }
    
    public function logout() {
        $this->Auth->authError ='';
    	if( $this->Session->read('Auth.User.id') > 0 ){
            $this->User->id = $this->Session->read('Auth.User.id');
            $this->User->saveField('last_login', date("Y-m-d H:i:s",strtotime("now")) );
	}
    	$this->Session->destroy();
        $this->redirect(array('controller'=>'/'));
    }
    
    public function paginate($object = null, $scope = array(), $whitelist = array(), $key = null) {
      	$results = parent::paginate($object, $scope, $whitelist);
      	if ($key) {
            $this->params['paging'][$key] = $this->params['paging'][$object];
            unset($this->params['paging'][$object]);
      	}
	return $results;
    }
}