<?php

class SteinlagerBarsController extends AppController {
    
    public $components = array(
    //    'DebugKit.Toolbar',
        'ImageResize'
    );
    
    public $name = 'SteinlagerBars';
    public $uses = array('SteinlagerBars');
    public $helpers = array(
        'TidyFilter'
    );
    
    public $paginate = array(
        'SteinlagerBars' => array(
            'conditions' => array('SteinlagerBars.status'=> 1),
            'limit'	 => 20,
            'page'	 => 1,
            'order'	 => array('SteinlagerBars.title' => 'ASC')
        )
    );
    
    private $options = array();
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->options = array(
            'script_url' => $_SERVER['PHP_SELF'],
            'upload_destination_dir' => dirname(dirname(dirname(__FILE__))).'/app/webroot/uploads/original/steinlager_bars/',
            'upload_dir' => dirname(dirname(dirname(__FILE__))).'/app/webroot/uploads/original/steinlager_bars/raw/',
            'upload_url' => dirname(dirname(dirname(__FILE__))).'/app/webroot/uploads/original/steinlager_bars/raw/',
            'param_name' => 'files',
            'image_width' => 640,
            'image_height' => 960,
            'max_file_size' => null,
            'min_file_size' => 1,
            'accept_file_types' => '/(\.|\/)(png|gif|jpg|jpeg)$/i',
            'max_number_of_files' => null,
            'discard_aborted_uploads' => true,
            'image_versions' => array(
                'thumbnail' => array(
                    'upload_dir' => dirname(dirname(dirname(__FILE__))).'/app/webroot/uploads/thumbs/480_320/steinlager_bars/',
                    'upload_url' => dirname(dirname(dirname(__FILE__))).'/app/webroot/uploads/thumbs/480_320/steinlager_bars/',
                    'max_width' => 320,
                    'max_height' => 480
                )
            )
        );
        Configure::write('debug',2);
    }

    public function index() {
        $this->paginate['SteinlagerBars'] = array(
            'conditions'=>  array('SteinlagerBars.status'=> Configure::read('status_live')),
            'order' => $this->paginate['SteinlagerBars']['order']
        );
        $this->set('steinlagerbars',$this->paginate('SteinlagerBars'));
    }
    
    public function search(){
        //echo 'went here';
    }
    
    public function add(){
        if( $this->RequestHandler->isPost() ){
            if( $this->validateFormPost() ){
                $this->SteinlagerBars->begin(); 
                $imagenotification = null;
                if( isset($this->data['SteinlagerBars']['image']['name']) && 
                    !empty($this->data['SteinlagerBars']['image']['name']) &&
                    intval($this->data['SteinlagerBars']['image']['error']) < 1    
                ){
                    if( $file = $this->validateFormImage() ){
                        if( !isset($file->message) ){    
                            $this->data['SteinlagerBars']['image'] = $file->name;
                        }else{
                            unset($this->data['SteinlagerBars']['image']);
                            $imagenotification = $file->message;
                        }
                    }
                }else{
                    unset($this->data['SteinlagerBars']['image']);
                }
                
                if( is_null($imagenotification) ){
                    $this->data['SteinlagerBars']['status'] = Configure::read('status_live');
                    $this->data['SteinlagerBars']['created_at'] = date("Y-m-d H:i:s",strtotime("now"));
                    $this->data['SteinlagerBars']['updated_at'] = date("Y-m-d H:i:s",strtotime("now"));
                    if ($this->SteinlagerBars->save($this->data)) {
                        $this->SteinlagerBars->commit();
                        $id = $this->SteinlagerBars->getLastInsertID();
                        $conditions = array(
                            'conditions' => array('SteinlagerBars.status' => Configure::read("status_live") ),   
                            'fields' => array(
                                'SteinlagerBars.id',
                                'SteinlagerBars.title',
                                'SteinlagerBars.address',
                                'SteinlagerBars.phone',
                                'SteinlagerBars.details',
                                'SteinlagerBars.image',
                                'SteinlagerBars.is_pure_bar',
                                'SteinlagerBars.latitude',
                                'SteinlagerBars.longitude',
                                'SteinlagerBars.facebook_id'
                            ),
                            'order' => array('SteinlagerBars.title ASC')
                        );
                        $steinlagers = $this->SteinlagerBars->find('all',$conditions);
                        Cache::write('steinlagers', $steinlagers);
                        $this->Session->setFlash(__('You have successfully added Steinlager Bar.', true), 'default',array('class' => 'alert alert-success'));
                        $this->redirect(array('controller' => 'steinlager_bars', 'action' => 'edit', $id. DIRECTORY_SEPARATOR . convertToSeoUri(strtolower(trim($this->data['SteinlagerBars']['title'])))));
                    }
                }else{//error on the image
                    $this->set('steinlagers',$this->data);
                    $this->Session->setFlash(__($imagenotification, true), 'default', array('class' => 'alert alert-error'));
                }
                
            }else{
                $this->set('steinlagers',$this->data);
                $this->Session->setFlash(__('One of the required fields is empty.', true), 'default', array('class' => 'alert alert-error'));
            }
        }
    }
    
    public function batch_delete(){
        Configure::write('debug',0);
        $this->layout = false;
        if( $this->RequestHandler->isPost() && isset($this->data['SteinlagerBars']['id']) && sizeof($this->data['SteinlagerBars']['id']) > 0 ){
            $conditions = array(
                'conditions' => array(
                    'SteinlagerBars.id IN ('.implode(",",$this->data['SteinlagerBars']['id']).')',
                    'SteinlagerBars.status' => Configure::read('status_live')
                ),
                'fields' => array('SteinlagerBars.id')
            );
            $steinlagers = $this->SteinlagerBars->find('all',$conditions);
            if( $steinlagers && sizeof($steinlagers)>0 && is_array($steinlagers) ){
                foreach( $steinlagers as $steinlager ){
                    $this->SteinlagerBars->begin(); 
                    $this->data['SteinlagerBars']['id'] = intval($steinlager['SteinlagerBars']['id']);
                    $this->data['SteinlagerBars']['status'] = Configure::read('status_deleted');
                    $this->data['SteinlagerBars']['updated_at'] = date("Y-m-d H:i:s",strtotime("now"));
                    if ($this->SteinlagerBars->save($this->data)) {
                        $this->SteinlagerBars->commit();
                    }
                }
                
                $conditions = array(
                    'conditions' => array('SteinlagerBars.status' => Configure::read("status_live") ),   
                    'fields' => array(
                        'SteinlagerBars.id',
                        'SteinlagerBars.title',
                        'SteinlagerBars.address',
                        'SteinlagerBars.phone',
                        'SteinlagerBars.details',
                        'SteinlagerBars.image',
                        'SteinlagerBars.is_pure_bar',
                        'SteinlagerBars.latitude',
                        'SteinlagerBars.longitude',
                        'SteinlagerBars.facebook_id'
                    ),
                    'order' => array('SteinlagerBars.title ASC')
                );
                $steinlagers = $this->SteinlagerBars->find('all',$conditions);
                Cache::write('steinlagers', $steinlagers);
                $this->Session->setFlash(__('Delete successful.', true), 'default',array('class' => 'alert alert-success'));
                $this->redirect(array("controller"=>"steinlager-bars"));
                die;
            }
        }
        $this->Session->setFlash(__('Invalid request please try again.', true), 'default', array('class' => 'alert alert-error'));
        $this->redirect(array("controller"=>"steinlager-bars"));
        die;
    }
    
    public function delete_image($id){
        Configure::write('debug',0);
        $this->layout = false;
        $steinlager = $this->SteinlagerBars->findById($id);
        if( intval($steinlager['SteinlagerBars']['status']) == Configure::read('status_live') ){
            $this->SteinlagerBars->begin(); 
            $this->data['SteinlagerBars']['id'] = $id;
            $this->data['SteinlagerBars']['image'] = NULL;
            $this->data['SteinlagerBars']['updated_at'] = date("Y-m-d H:i:s",strtotime("now"));
            if ($this->SteinlagerBars->save($this->data)) {
                $this->SteinlagerBars->commit();
                $conditions = array(
                    'conditions' => array('SteinlagerBars.status' => Configure::read("status_live") ),   
                    'fields' => array(
                        'SteinlagerBars.id',
                        'SteinlagerBars.title',
                        'SteinlagerBars.address',
                        'SteinlagerBars.phone',
                        'SteinlagerBars.details',
                        'SteinlagerBars.image',
                        'SteinlagerBars.is_pure_bar',
                        'SteinlagerBars.latitude',
                        'SteinlagerBars.longitude',
                        'SteinlagerBars.facebook_id'
                    ),
                    'order' => array('SteinlagerBars.title ASC')
                );
                $steinlagers = $this->SteinlagerBars->find('all',$conditions);
                Cache::write('steinlagers', $steinlagers);
                $this->Session->setFlash(__('You have successfully deleted the image for "'.$steinlager['SteinlagerBars']['title'].'"', true), 'default',array('class' => 'alert alert-success'));
            }else{
                $this->Session->setFlash(__('Invalid delete request', true), 'default', array('class' => 'alert alert-error'));
            }
        }else{
            $this->Session->setFlash(__('Invalid delete request', true), 'default', array('class' => 'alert alert-error'));
        }
        $this->redirect(array("controller"=>"steinlager_bars",'action'=> 'edit',intval($steinlager['SteinlagerBars']['id']).DIRECTORY_SEPARATOR.convertToSeoUri(trim($steinlager['SteinlagerBars']['title']))));
        die;
    }
    
    public function delete($id){
        Configure::write('debug',0);
        $this->layout = false;
        $steinlager = $this->SteinlagerBars->findById($id);
        if( intval($steinlager['SteinlagerBars']['status']) == Configure::read('status_live') ){
            $this->SteinlagerBars->begin(); 
            $this->data['SteinlagerBars']['id'] = $id;
            $this->data['SteinlagerBars']['status'] = Configure::read('status_deleted');
            $this->data['SteinlagerBars']['updated_at'] = date("Y-m-d H:i:s",strtotime("now"));
            if ($this->SteinlagerBars->save($this->data)) {
                $this->SteinlagerBars->commit();
                $conditions = array(
                    'conditions' => array('SteinlagerBars.status' => Configure::read("status_live") ),   
                    'fields' => array(
                        'SteinlagerBars.id',
                        'SteinlagerBars.title',
                        'SteinlagerBars.address',
                        'SteinlagerBars.phone',
                        'SteinlagerBars.details',
                        'SteinlagerBars.image',
                        'SteinlagerBars.is_pure_bar',
                        'SteinlagerBars.latitude',
                        'SteinlagerBars.longitude',
                        'SteinlagerBars.facebook_id'
                    ),
                    'order' => array('SteinlagerBars.title ASC')
                );
                $steinlagers = $this->SteinlagerBars->find('all',$conditions);
                Cache::write('steinlagers', $steinlagers);
                $this->Session->setFlash(__('You have successfully deleted "'.$steinlager['SteinlagerBars']['title'].'"', true), 'default',array('class' => 'alert alert-success'));
            }else{
                $this->Session->setFlash(__('Invalid delete request', true), 'default', array('class' => 'alert alert-error'));
            }
        }else{
            $this->Session->setFlash(__('Invalid delete request', true), 'default', array('class' => 'alert alert-error'));
        }
        $this->redirect(array("controller"=>"steinlager-bars"));
        die;
    }
    
    public function edit_geocode(){
        Configure::write('debug',0);
        $this->layout = false;
        $id = intval($this->params['url']['id']);
        $steinlager = $this->SteinlagerBars->findById($id);
        if( intval($steinlager['SteinlagerBars']['status']) == Configure::read('status_live') ){
            $this->SteinlagerBars->begin(); 
            $this->data['SteinlagerBars']['id'] = $id;
            $this->data['SteinlagerBars']['latitude'] = trim($this->params['url']['latitude']);
            $this->data['SteinlagerBars']['longitude'] = trim($this->params['url']['longitude']);
            $this->data['SteinlagerBars']['updated_at'] = date("Y-m-d H:i:s",strtotime("now"));
            if ($this->SteinlagerBars->save($this->data)) {
                $this->SteinlagerBars->commit();
                $conditions = array(
                    'conditions' => array('SteinlagerBars.status' => Configure::read("status_live") ),   
                    'fields' => array(
                        'SteinlagerBars.id',
                        'SteinlagerBars.title',
                        'SteinlagerBars.address',
                        'SteinlagerBars.phone',
                        'SteinlagerBars.details',
                        'SteinlagerBars.image',
                        'SteinlagerBars.is_pure_bar',
                        'SteinlagerBars.latitude',
                        'SteinlagerBars.longitude',
                        'SteinlagerBars.facebook_id'
                    ),
                    'order' => array('SteinlagerBars.title ASC')
                );
                $steinlagers = $this->SteinlagerBars->find('all',$conditions);
                Cache::write('steinlagers', $steinlagers);
                echo json_encode(true);
            }
        }
        die;
    }
    
    public function edit($id){
        $steinlagers = $this->SteinlagerBars->findById($id);
        if( intval($steinlagers['SteinlagerBars']['status']) == Configure::read('status_live') ){
            if( $this->RequestHandler->isPost() || $this->RequestHandler->isPut() ){
                if( $this->validateFormPost() ){
                    $this->SteinlagerBars->begin(); 
                    
                    $imagenotification = null;
                    if( isset($this->data['SteinlagerBars']['image']['name']) && 
                        !empty($this->data['SteinlagerBars']['image']['name']) &&
                        intval($this->data['SteinlagerBars']['image']['error']) < 1    
                    ){
                        if( $file = $this->validateFormImage() ){
                            if( !isset($file->message) ){    
                                $this->data['SteinlagerBars']['image'] = $file->name;
                            }else{
                                unset($this->data['SteinlagerBars']['image']);
                                $imagenotification = $file->message;
                            }
                        }
                    }else{
                        unset($this->data['SteinlagerBars']['image']);
                    }

                    if( is_null($imagenotification) ){
                    
                        $this->data['SteinlagerBars']['status'] = Configure::read('status_live');
                        $this->data['SteinlagerBars']['updated_at'] = date("Y-m-d H:i:s",strtotime("now"));
                        if ($this->SteinlagerBars->save($this->data)) {
                            $this->SteinlagerBars->commit();
                            $id = intval($this->data['SteinlagerBars']['id']);
                            $conditions = array(
                                'conditions' => array('SteinlagerBars.status' => Configure::read("status_live") ),   
                                'fields' => array(
                                    'SteinlagerBars.id',
                                    'SteinlagerBars.title',
                                    'SteinlagerBars.address',
                                    'SteinlagerBars.phone',
                                    'SteinlagerBars.details',
                                    'SteinlagerBars.image',
                                    'SteinlagerBars.is_pure_bar',
                                    'SteinlagerBars.latitude',
                                    'SteinlagerBars.longitude',
                                    'SteinlagerBars.facebook_id'
                                ),
                                'order' => array('SteinlagerBars.title ASC')
                            );
                            $steinlagers = $this->SteinlagerBars->find('all',$conditions);
                            Cache::write('steinlagers', $steinlagers);
                            $this->Session->setFlash(__('You have successfully edited '.$this->data['SteinlagerBars']['title'], true), 'default',array('class' => 'alert alert-success'));
                            $this->redirect(array('controller' => 'steinlager_bars', 'action' => 'edit', $id. DIRECTORY_SEPARATOR . convertToSeoUri(strtolower(trim($this->data['SteinlagerBars']['title'])))));
                        }
                    }else{
                        $this->set('post_steinlagers',$this->data);
                        $this->Session->setFlash(__($imagenotification, true), 'default', array('class' => 'alert alert-error'));
                    }
                }else{
                    $this->Session->setFlash(__('One of the required fields is empty.', true), 'default', array('class' => 'alert alert-error'));
                }
                $this->set('post_steinlagers', $this->data);
            }
            $this->set('steinlagers', $steinlagers);
            $this->set('title_for_layout','Edit Top 50 '.ucwords(strtolower($steinlagers['SteinlagerBars']['title'])));
        }else{
            
            $this->Session->setFlash(__('Invalid request', true), 'default', array('class' => 'alert alert-error'));
            $this->redirect('/');
        }
    }
    
    private function validateFormPost(){
        if( (strlen(trim($this->data['SteinlagerBars']['title']))     > 0) &&
            (strlen(trim($this->data['SteinlagerBars']['address']))   > 0) &&
            (strlen(trim($this->data['SteinlagerBars']['details']))   > 0) &&
            (strlen(trim($this->data['SteinlagerBars']['latitude']))  > 0) &&
            (strlen(trim($this->data['SteinlagerBars']['longitude'])) > 0)     
        ){
            return true;
        }else{
            return false;
        }
    }
    
    private function validateFormImage($validateWithImage=true){
        $notification = new stdClass();
        if( intval($this->data['SteinlagerBars']['image']['error']) < 1 ){
            list($width, $height, $type, $attr) = @getimagesize($this->data['SteinlagerBars']['image']['tmp_name']);
            if( intval($width) == intval($this->options['image_width']) && intval($height) == intval($this->options['image_height']) ){
                $file_uploaded = $this->handle_file_upload(
                    $this->data['SteinlagerBars']['image']['tmp_name'],
                    $this->data['SteinlagerBars']['image']['name'],
                    $this->data['SteinlagerBars']['image']['size'],
                    $this->data['SteinlagerBars']['image']['type'],
                    $this->data['SteinlagerBars']['image']['error']
                );

                if( is_object($file_uploaded) && isset($file_uploaded->size) && intval($file_uploaded->size)>0 && !isset($file_uploaded->error) ){
                    $src = $this->options['upload_dir'].$file_uploaded->name;
                    $destination = $this->options['upload_destination_dir'].$file_uploaded->name;
                    $thumbs = $this->options['image_versions']['thumbnail']['upload_dir'].'thumbs_'.$file_uploaded->name;
                    $thumb_width = intval($this->options['image_versions']['thumbnail']['max_width']);
                    $thumb_height = intval($this->options['image_versions']['thumbnail']['max_height']);
                    $errors = $this->ImageResize->resize($src, $destination, $dim_setting=RESIZE_BOTH, $width = $this->options['image_width'], $this->options['image_height']);//big
                    if( !$errors ){
                        $errors = $this->ImageResize->resize($src, $thumbs, $dim_setting=RESIZE_BOTH, $width = $thumb_width, $thumb_height);//thumbnail 
                        $notification = $file_uploaded;
                    }else{
                        $notification->message = $errors;
                    }
                }else{

                    $notification->message = $file_uploaded->error;
                }

            }else{
                $notification->message = 'Image dimensions '.$width.' x '.$height.' does not comply the standard size of '.$this->options['image_width'].' x '.$this->options['image_height'];
            }
        }else{
            $notification->message = 'Please upload valid image file. Please try again.';
        }
        return $notification;
    }
    
    private function handle_file_upload($uploaded_file, $name, $size, $type, $error) {
        
        $file = new stdClass();
        $file->name = trim(basename(stripslashes($name)), ".\x00..\x20");
        $extension  = pathinfo($file->name, PATHINFO_EXTENSION);
        $file->name = md5(strtotime("now")).'.jpg';
        $file->size = intval($size);
        $file->type = $type;
        
        $error = $this->has_error($uploaded_file, $file, $error);
        
        if (!$error && $file->name) {
            
            $file_path = $this->options['upload_dir'].$file->name;
            $append_file = is_file($file_path) && $file->size > filesize($file_path);
            clearstatcache();
            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                if ($append_file) {
                    file_put_contents(
                        $file_path,
                        fopen($uploaded_file, 'r'),
                        FILE_APPEND
                    );
                } else {
                    move_uploaded_file($uploaded_file, $file_path);
                }
            } else {
                // Non-multipart uploads (PUT method support)
                file_put_contents(
                    $file_path,
                    fopen('php://input', 'r'),
                    $append_file ? FILE_APPEND : 0
                );
            }

            $old = umask(0);
            chmod($file_path, 0777);
            umask($old);
        } else {
            $file->error = $error;
        }
        return $file;
    }
    
    private function has_error($uploaded_file, $file, $error) {
        if ($error) {
            return $error;
        }
        if (!preg_match($this->options['accept_file_types'], $file->name)) {
            return 'acceptFileTypes';
        }
        if ($uploaded_file && is_uploaded_file($uploaded_file)) {
            $file_size = filesize($uploaded_file);
        } else {
            $file_size = $_SERVER['CONTENT_LENGTH'];
        }
        if ($this->options['max_file_size'] && (
                $file_size > $this->options['max_file_size'] ||
                $file->size > $this->options['max_file_size'])
            ) {
            return 'maxFileSize';
        }
        if ($this->options['min_file_size'] &&
            $file_size < $this->options['min_file_size']) {
            return 'minFileSize';
        }
        
        return $error;
    }
}
