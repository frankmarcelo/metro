<?php

class CheapEatsController extends AppController {
    
    public $components = array(
    //    'DebugKit.Toolbar',
        'ImageResize'
    );
    
    public $name = 'CheapEats';
    public $uses = array('CheapEats');
    public $helpers = array(
        'TidyFilter'
    );
    
    public $paginate = array(
        'CheapEats'	=> array(
            'conditions' => array('CheapEats.status'=> 1),
            'limit'	=> 20,
            'page'	=> 1,
            'order'	=> array('CheapEats.title' => 'ASC')
        )
    );
    
    private $options = array();
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->options = array(
            'script_url' => $_SERVER['PHP_SELF'],
            'upload_destination_dir' => dirname(dirname(dirname(__FILE__))).'/app/webroot/uploads/original/cheap_eats/',
            'upload_dir' => dirname(dirname(dirname(__FILE__))).'/app/webroot/uploads/original/cheap_eats/raw/',
            'upload_url' => dirname(dirname(dirname(__FILE__))).'/app/webroot/uploads/original/cheap_eats/raw/',
            'param_name' => 'files',
            'image_width' => 640,
            'image_height' => 960,
            'max_file_size' => null,
            'min_file_size' => 1,
            'accept_file_types' => '/(\.|\/)(png|gif|jpg|jpeg)$/i',
            'max_number_of_files' => null,
            'discard_aborted_uploads' => true,
            'image_versions' => array(
                'thumbnail' => array(
                    'upload_dir' => dirname(dirname(dirname(__FILE__))).'/app/webroot/uploads/thumbs/480_320/cheap_eats/',
                    'upload_url' => dirname(dirname(dirname(__FILE__))).'/app/webroot/uploads/thumbs/480_320/cheap_eats/',
                    'max_width' => 320,
                    'max_height' => 480
                )
            )
        );
        Configure::write('debug',2);
    }

    public function index() {
        $this->paginate['CheapEats'] = array(
            'conditions'=>  array('CheapEats.status'=> Configure::read('status_live')),
            'order' => $this->paginate['CheapEats']['order'],
            'limit' => $this->paginate['CheapEats']['limit']
        );
        $this->set('cheapeats',$this->paginate('CheapEats'));
    }
    
    public function search(){
        //echo 'went here';
    }
    
    public function add(){
        if( $this->RequestHandler->isPost() ){
            if( $this->validateFormPost() ){
                $this->CheapEats->begin(); 
                
                $imagenotification = null;
                if( isset($this->data['CheapEats']['image']['name']) && 
                    !empty($this->data['CheapEats']['image']['name']) &&
                    intval($this->data['CheapEats']['image']['error']) < 1    
                ){
                    if( $file = $this->validateFormImage() ){
                        if( !isset($file->message) ){    
                            $this->data['CheapEats']['image'] = $file->name;
                        }else{
                            unset($this->data['CheapEats']['image']);
                            $imagenotification = $file->message;
                        }
                    }
                }else{
                    unset($this->data['CheapEats']['image']);
                }
                
                if( is_null($imagenotification) ){
                    $this->data['CheapEats']['status'] = Configure::read('status_live');
                    $this->data['CheapEats']['created_at'] = date("Y-m-d H:i:s",strtotime("now"));
                    $this->data['CheapEats']['updated_at'] = date("Y-m-d H:i:s",strtotime("now"));
                    if ($this->CheapEats->save($this->data)) {
                        $this->CheapEats->commit();
                        $id = $this->CheapEats->getLastInsertID();
                        $conditions = array(
                            'conditions' => array('CheapEats.status' => Configure::read("status_live") ),   
                            'fields' => array(
                                'CheapEats.id',
                                'CheapEats.title',
                                'CheapEats.address',
                                'CheapEats.phone',
                                'CheapEats.details',
                                'CheapEats.style_of_food',
                                'CheapEats.image',
                                'CheapEats.latitude',
                                'CheapEats.longitude',
                                'CheapEats.facebook_id'
                            ),
                            'order' => array('CheapEats.title ASC')
                        );
                        $cheapeats = $this->CheapEats->find('all',$conditions);
                        Cache::write('cheapeats', $cheapeats);
                        $this->Session->setFlash(__('You have successfully added Cheap Eats restaurant.', true), 'default',array('class' => 'alert alert-success'));
                        $this->redirect(array('controller' => 'cheap_eats', 'action' => 'edit', $id. DIRECTORY_SEPARATOR . convertToSeoUri(strtolower(trim($this->data['CheapEats']['title'])))));
                    }
                }else{//error on the image
                    $this->set('cheapeats',$this->data);
                    $this->Session->setFlash(__($imagenotification, true), 'default', array('class' => 'alert alert-error'));
                }
                
            }else{
                $this->set('cheapeats',$this->data);
                $this->Session->setFlash(__('One of the required fields is empty.', true), 'default', array('class' => 'alert alert-error'));
            }
        }
    }
    
    public function batch_delete(){
        Configure::write('debug',0);
        $this->layout = false;
        if( $this->RequestHandler->isPost() && isset($this->data['CheapEats']['id']) && sizeof($this->data['CheapEats']['id']) > 0 ){
            $conditions = array(
                'conditions' => array(
                    'CheapEats.id IN ('.implode(",",$this->data['CheapEats']['id']).')',
                    'CheapEats.status' => Configure::read('status_live')
                ),
                'fields' => array('CheapEats.id')
            );
            $cheapeats = $this->CheapEats->find('all',$conditions);
            if( $cheapeats && sizeof($cheapeats)>0 && is_array($cheapeats) ){
                foreach( $cheapeats as $cheapeat ){
                    $this->CheapEats->begin(); 
                    $this->data['CheapEats']['id'] = intval($cheapeat['CheapEats']['id']);
                    $this->data['CheapEats']['status'] = Configure::read('status_deleted');
                    $this->data['CheapEats']['updated_at'] = date("Y-m-d H:i:s",strtotime("now"));
                    if ($this->CheapEats->save($this->data)) {
                        $this->CheapEats->commit();
                    }
                }
                
                $conditions = array(
                    'conditions' => array('CheapEats.status' => Configure::read("status_live") ),   
                    'fields' => array(
                        'CheapEats.id',
                        'CheapEats.title',
                        'CheapEats.address',
                        'CheapEats.phone',
                        'CheapEats.details',
                        'CheapEats.style_of_food',
                        'CheapEats.image',
                        'CheapEats.latitude',
                        'CheapEats.longitude',
                        'CheapEats.facebook_id'
                    ),
                    'order' => array('CheapEats.title ASC')
                );
                $cheapeats = $this->CheapEats->find('all',$conditions);
                Cache::write('cheapeats', $cheapeats);
                $this->Session->setFlash(__('Delete successful.', true), 'default',array('class' => 'alert alert-success'));
                $this->redirect(array("controller"=>"cheap-eats"));
                die;
            }
        }
        $this->Session->setFlash(__('Invalid request please try again.', true), 'default', array('class' => 'alert alert-error'));
        $this->redirect(array("controller"=>"cheap-eats"));
        die;
    }
    
    public function delete_image($id){
        
        Configure::write('debug',0);
        $this->layout = false;
        $cheapeat = $this->CheapEats->findById($id);
        if( intval($cheapeat['CheapEats']['status']) == Configure::read('status_live') ){
            $this->CheapEats->begin(); 
            $this->data['CheapEats']['id'] = $id;
            $this->data['CheapEats']['image'] = NULL;
            $this->data['CheapEats']['updated_at'] = date("Y-m-d H:i:s",strtotime("now"));
            if ($this->CheapEats->save($this->data)) {
                $this->CheapEats->commit();
                $conditions = array(
                    'conditions' => array('CheapEats.status' => Configure::read("status_live") ),   
                    'fields' => array(
                        'CheapEats.id',
                        'CheapEats.title',
                        'CheapEats.address',
                        'CheapEats.phone',
                        'CheapEats.details',
                        'CheapEats.style_of_food',
                        'CheapEats.image',
                        'CheapEats.latitude',
                        'CheapEats.longitude',
                        'CheapEats.facebook_id'
                    ),
                    'order' => array('CheapEats.title ASC')
                );
                $cheapeats = $this->CheapEats->find('all',$conditions);
                Cache::write('cheapeats', $cheapeats);
                $this->Session->setFlash(__('You have successfully deleted the image for "'.$cheapeat['CheapEats']['title'].'"', true), 'default',array('class' => 'alert alert-success'));
            }else{
                $this->Session->setFlash(__('Invalid delete request', true), 'default', array('class' => 'alert alert-error'));
            }
        }else{
            $this->Session->setFlash(__('Invalid delete request', true), 'default', array('class' => 'alert alert-error'));
        }
        $this->redirect(array("controller"=>"cheap_eats",'action'=>'edit',intval($cheapeat['CheapEats']['id']).DIRECTORY_SEPARATOR.convertToSeoUri(trim($cheapeat['CheapEats']['title']))));
        die;
    }
    
    public function delete($id){
        Configure::write('debug',0);
        $this->layout = false;
        $cheapeat = $this->CheapEats->findById($id);
        if( intval($cheapeat['CheapEats']['status']) == Configure::read('status_live') ){
            $this->CheapEats->begin(); 
            $this->data['CheapEats']['id'] = $id;
            $this->data['CheapEats']['status'] = Configure::read('status_deleted');
            $this->data['CheapEats']['updated_at'] = date("Y-m-d H:i:s",strtotime("now"));
            if ($this->CheapEats->save($this->data)) {
                $this->CheapEats->commit();
                $conditions = array(
                    'conditions' => array('CheapEats.status' => Configure::read("status_live") ),   
                    'fields' => array(
                        'CheapEats.id',
                        'CheapEats.title',
                        'CheapEats.address',
                        'CheapEats.phone',
                        'CheapEats.details',
                        'CheapEats.style_of_food',
                        'CheapEats.image',
                        'CheapEats.latitude',
                        'CheapEats.longitude',
                        'CheapEats.facebook_id'
                    ),
                    'order' => array('CheapEats.title ASC')
                );
                $cheapeats = $this->CheapEats->find('all',$conditions);
                Cache::write('cheapeats', $cheapeats);
                $this->Session->setFlash(__('You have successfully deleted "'.$cheapeat['CheapEats']['title'].'"', true), 'default',array('class' => 'alert alert-success'));
            }else{
                $this->Session->setFlash(__('Invalid delete request', true), 'default', array('class' => 'alert alert-error'));
            }
        }else{
            $this->Session->setFlash(__('Invalid delete request', true), 'default', array('class' => 'alert alert-error'));
        }
        $this->redirect(array("controller"=>"cheap-eats"));
        die;
    }
    
    public function edit_geocode(){
        Configure::write('debug',0);
        $this->layout = false;
        $id = intval($this->params['url']['id']);
        $cheapeat = $this->CheapEats->findById($id);
        if( intval($cheapeat['CheapEats']['status']) == Configure::read('status_live') ){
            $this->CheapEats->begin(); 
            $this->data['CheapEats']['id'] = $id;
            $this->data['CheapEats']['latitude'] = trim($this->params['url']['latitude']);
            $this->data['CheapEats']['longitude'] = trim($this->params['url']['longitude']);
            $this->data['CheapEats']['updated_at'] = date("Y-m-d H:i:s",strtotime("now"));
            if ($this->CheapEats->save($this->data)) {
                $this->CheapEats->commit();
                $id = intval($this->data['CheapEats']['id']);
                $conditions = array(
                    'conditions' => array('CheapEats.status' => Configure::read("status_live") ),   
                    'fields' => array(
                        'CheapEats.id',
                        'CheapEats.title',
                        'CheapEats.address',
                        'CheapEats.phone',
                        'CheapEats.details',
                        'CheapEats.style_of_food',
                        'CheapEats.image',
                        'CheapEats.latitude',
                        'CheapEats.longitude',
                        'CheapEats.facebook_id'
                    ),
                    'order' => array('CheapEats.title ASC')
                );
                $cheapeats = $this->CheapEats->find('all',$conditions);
                Cache::write('cheapeats', $cheapeats);
                echo json_encode(true);
            }
        }
        die;
    }
    
    public function edit($id){
        $cheapeats = $this->CheapEats->findById($id);
        if( intval($cheapeats['CheapEats']['status']) == Configure::read('status_live') ){
            if( $this->RequestHandler->isPost() || $this->RequestHandler->isPut() ){
                if( $this->validateFormPost() ){
                    $this->CheapEats->begin(); 
                    $imagenotification = null;
                    if( isset($this->data['CheapEats']['image']['name']) && 
                        !empty($this->data['CheapEats']['image']['name']) &&
                        intval($this->data['CheapEats']['image']['error']) < 1    
                    ){
                        if( $file = $this->validateFormImage() ){
                            if( !isset($file->message) ){    
                                $this->data['CheapEats']['image'] = $file->name;
                            }else{
                                unset($this->data['CheapEats']['image']);
                                $imagenotification = $file->message;
                            }
                        }
                    }else{
                        unset($this->data['CheapEats']['image']);
                    }

                    if( is_null($imagenotification) ){
                        $this->data['CheapEats']['status'] = Configure::read('status_live');
                        $this->data['CheapEats']['updated_at'] = date("Y-m-d H:i:s",strtotime("now"));
                        if ($this->CheapEats->save($this->data)) {
                            $this->CheapEats->commit();
                            $id = intval($this->data['CheapEats']['id']);
                            $conditions = array(
                                'conditions' => array('CheapEats.status' => Configure::read("status_live") ),   
                                'fields' => array(
                                    'CheapEats.id',
                                    'CheapEats.title',
                                    'CheapEats.address',
                                    'CheapEats.phone',
                                    'CheapEats.details',
                                    'CheapEats.style_of_food',
                                    'CheapEats.image',
                                    'CheapEats.latitude',
                                    'CheapEats.longitude',
                                    'CheapEats.facebook_id'
                                ),
                                'order' => array('CheapEats.title ASC')
                            );
                            $cheapeats = $this->CheapEats->find('all',$conditions);
                            Cache::write('cheapeats', $cheapeats);
                            $this->Session->setFlash(__('You have successfully edited '.$this->data['CheapEats']['title'], true), 'default',array('class' => 'alert alert-success'));
                            $this->redirect(array('controller' => 'cheap_eats', 'action' => 'edit', $id. DIRECTORY_SEPARATOR . convertToSeoUri(strtolower(trim($this->data['CheapEats']['title'])))));
                        }
                    }else{
                        $this->set('post_cheapeats',$this->data);
                        $this->Session->setFlash(__($imagenotification, true), 'default', array('class' => 'alert alert-error'));
                    }
                }else{
                    $this->Session->setFlash(__('One of the required fields is empty.', true), 'default', array('class' => 'alert alert-error'));
                }
                $this->set('post_cheapeats', $this->data);
            }
            $this->set('cheapeats', $cheapeats);
            $this->set('title_for_layout','Edit Top 50 '.ucwords(strtolower($cheapeats['CheapEats']['title'])));
        }else{
            
            $this->Session->setFlash(__('Invalid request', true), 'default', array('class' => 'alert alert-error'));
            $this->redirect('/');
        }
    }
    
    private function validateFormPost(){
        if( (strlen(trim($this->data['CheapEats']['title']))         > 0) &&
            (strlen(trim($this->data['CheapEats']['address']))       > 0) &&
            (strlen(trim($this->data['CheapEats']['details']))       > 0) &&
            (strlen(trim($this->data['CheapEats']['style_of_food'])) > 0) &&
            (strlen(trim($this->data['CheapEats']['latitude']))      > 0) &&
            (strlen(trim($this->data['CheapEats']['longitude']))     > 0)     
        ){
            return true;
        }else{
            return false;
        }
    }
    
    private function validateFormImage($validateWithImage=true){
        $notification = new stdClass();
        if( intval($this->data['CheapEats']['image']['error']) < 1 ){
            list($width, $height, $type, $attr) = @getimagesize($this->data['CheapEats']['image']['tmp_name']);
            if( intval($width) >= intval($this->options['image_width']) && intval($height) >= intval($this->options['image_height']) ){
                $file_uploaded = $this->handle_file_upload(
                    $this->data['CheapEats']['image']['tmp_name'],
                    $this->data['CheapEats']['image']['name'],
                    $this->data['CheapEats']['image']['size'],
                    $this->data['CheapEats']['image']['type'],
                    $this->data['CheapEats']['image']['error']
                );

                if( is_object($file_uploaded) && isset($file_uploaded->size) && intval($file_uploaded->size)>0 && !isset($file_uploaded->error) ){
                    $src = $this->options['upload_dir'].$file_uploaded->name;
                    $destination = $this->options['upload_destination_dir'].$file_uploaded->name;
                    $thumbs = $this->options['image_versions']['thumbnail']['upload_dir'].'thumbs_'.$file_uploaded->name;
                    $thumb_width = intval($this->options['image_versions']['thumbnail']['max_width']);
                    $thumb_height = intval($this->options['image_versions']['thumbnail']['max_height']);
                    $errors = $this->ImageResize->resize($src, $destination, $dim_setting=RESIZE_BOTH, $width = $this->options['image_width'], $this->options['image_height']);//big
                    if( !$errors ){
                        $errors = $this->ImageResize->resize($src, $thumbs, $dim_setting=RESIZE_BOTH, $width = $thumb_width, $thumb_height);//thumbnail 
                        $notification = $file_uploaded;
                    }else{
                        $notification->message = $errors;
                    }
                }else{

                    $notification->message = $file_uploaded->error;
                }

            }else{
                $notification->message = 'Image dimensions '.$width.' x '.$height.' does not comply the standard size of '.$this->options['image_width'].' x '.$this->options['image_height'];
            }
        }else{
            $notification->message = 'Please upload valid image file. Please try again.';
        }
        return $notification;
    }
    
    private function handle_file_upload($uploaded_file, $name, $size, $type, $error) {
        
        $file = new stdClass();
        $file->name = trim(basename(stripslashes($name)), ".\x00..\x20");
        $extension  = pathinfo($file->name, PATHINFO_EXTENSION);
        $file->name = md5(strtotime("now")).'.jpg';
        $file->size = intval($size);
        $file->type = $type;
        
        $error = $this->has_error($uploaded_file, $file, $error);
        
        if (!$error && $file->name) {
            
            $file_path = $this->options['upload_dir'].$file->name;
            $append_file = is_file($file_path) && $file->size > filesize($file_path);
            clearstatcache();
            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                if ($append_file) {
                    file_put_contents(
                        $file_path,
                        fopen($uploaded_file, 'r'),
                        FILE_APPEND
                    );
                } else {
                    move_uploaded_file($uploaded_file, $file_path);
                }
            } else {
                // Non-multipart uploads (PUT method support)
                file_put_contents(
                    $file_path,
                    fopen('php://input', 'r'),
                    $append_file ? FILE_APPEND : 0
                );
            }

            $old = umask(0);
            chmod($file_path, 0777);
            umask($old);
        } else {
            $file->error = $error;
        }
        return $file;
    }
    
    private function has_error($uploaded_file, $file, $error) {
        if ($error) {
            return $error;
        }
        if (!preg_match($this->options['accept_file_types'], $file->name)) {
            return 'acceptFileTypes';
        }
        if ($uploaded_file && is_uploaded_file($uploaded_file)) {
            $file_size = filesize($uploaded_file);
        } else {
            $file_size = $_SERVER['CONTENT_LENGTH'];
        }
        if ($this->options['max_file_size'] && (
                $file_size > $this->options['max_file_size'] ||
                $file->size > $this->options['max_file_size'])
            ) {
            return 'maxFileSize';
        }
        if ($this->options['min_file_size'] &&
            $file_size < $this->options['min_file_size']) {
            return 'minFileSize';
        }
        
        return $error;
    }
}
