<?php

App::import('Sanitize');
App::import('Core','HttpSocket');

class WebservicesController extends AppController {
	
    public $name = 'Webservices';
    public $uses = array(
        'Log',
        'CheapEats',
        'SteinlagerBars',
        'TopRestaurants'
    );  
 
    private $version = 'v1.0'; 
    
    private $methodActions = array(
        'index',
        'CheapEats',
        'SteinlagerBars',
        'TopRestaurants',
    ); 
    
    public function beforeFilter() {
        Configure::write('debug',0);
        parent::beforeFilter();
        $this->Auth->allow(array('CheapEats','SteinlagerBars','TopRestaurants'));
        $this->Security->blackHoleCallback = 'forceSSL';
        $this->httpCodes();
        //$this->Security->requireSecure();
        //if( $this->RequestHandler->isSSL() ){
        $this->RequestHandler->respondAs('json');
        $this->RequestHandler->setContent('json','application/json');  
        $this->disableCache();
        $this->header('Content-Type: application/json');
        if( true ){
            if( $this->RequestHandler->isGet() ){
            }else{
                $this->throwExceptionError( $errorCode = 401, $errorMesage = Configure::read('httpBadRequestMessage'),__FUNCTION__ );
            }    
        
    	}else{
            $this->throwExceptionError( $errorCode = 403, $errorMesage = Configure::read('httpForbiddenAccessMessage'),__FUNCTION__ );
        }
    }

    public function forceSSL() {
        $this->redirect('https://' . env('SERVER_NAME') . $this->here);
    }

    public function index(){
        $this->header('HTTP/1.1 200 '.$this->__httpCodes[200]);
        $content_for_layout[] = array(
            'status' =>'Success' 
        );
        $this->set(compact('content_for_layout'));
    }

    private function requestLogs($args=array()){
    	$this->data['Log']['requestWith']= $this->RequestHandler->requestedWith();
    	$this->data['Log']['requestIP'] = $this->RequestHandler->getClientIP();
    	$this->data['Log']['requestMethod'] = $args['requestMethod'];
    	$this->data['Log']['requestType'] = ($this->RequestHandler->isPost()) ? 'post':'get';
    	$this->data['Log']['isMobile'] = ($this->RequestHandler->isMobile()) ? 1:0;
    	$this->data['Log']['created_at'] = date("Y-m-d H:i:s");
        $this->data['Log']['updated_at'] = date("Y-m-d H:i:s");
        if($this->Log->save($this->data)){
            $this->Log->commit();
            $id = $this->Log->query("SELECT LAST_INSERT_ID() as id");
            return $id[0][0]['id'];
    	}else{
            $this->Log->rollback();
            return 0;
        }
    }

    private function requestLogsUpdate($args=array(),$logId=0){
        $this->Log->id = $logId;
        $this->Log->begin();
        $this->data['Log']['requestRS'] = trim($args['requestRS']);
        $this->data['Log']['updated_at'] = date("Y-m-d H:i:s");
        if($this->Log->save($this->data)){
            $this->Log->commit();
        }
    }
    
    private function ucwords_specific ($string, $delimiters = '', $encoding = NULL) {
 
        if ($encoding === NULL) { $encoding = mb_internal_encoding();}
        if (is_string($delimiters)){
            $delimiters =  str_split( str_replace(' ', '', $delimiters));
        }

        $delimiters_pattern1 = array();
        $delimiters_replace1 = array();
        $delimiters_pattern2 = array();
        $delimiters_replace2 = array();
        
        foreach ($delimiters as $delimiter){
            $uniqid = uniqid();
            $delimiters_pattern1[]   = '/'. preg_quote($delimiter) .'/';
            $delimiters_replace1[]   = $delimiter.$uniqid.' ';
            $delimiters_pattern2[]   = '/'. preg_quote($delimiter.$uniqid.' ') .'/';
            $delimiters_replace2[]   = $delimiter;
        }

        // $return_string = mb_strtolower($string, $encoding);
        $return_string = $string;
        $return_string = preg_replace($delimiters_pattern1, $delimiters_replace1, $return_string);

        $words = explode(' ', $return_string);

        foreach ($words as $index => $word){
            $words[$index] = mb_strtoupper(mb_substr($word, 0, 1, $encoding), $encoding).mb_substr($word, 1, mb_strlen($word, $encoding), $encoding);
        }

        $return_string = implode(' ', $words);
        $return_string = preg_replace($delimiters_pattern2, $delimiters_replace2, $return_string);
        return $return_string;
    }  
    
    public function TopRestaurants(){
        Configure::write('debug',0);
        $transactionIdentifier = $this->requestLogs(array('requestMethod' => __FUNCTION__));
        $conditions = array(
            'conditions' => array('TopRestaurants.status' => Configure::read("status_live") ),   
            'fields' => array(
                'TopRestaurants.id',
                'TopRestaurants.title',
                'TopRestaurants.address',
                'TopRestaurants.phone',
                'TopRestaurants.details',
                'TopRestaurants.image',
                'TopRestaurants.quality_rating',
                'TopRestaurants.price_rating',
                'TopRestaurants.favourite_dish',
                'TopRestaurants.opening_hours',
                'TopRestaurants.latitude',
                'TopRestaurants.longitude',
                'TopRestaurants.facebook_id'
            ),
            'order' => array('TopRestaurants.title ASC')
        );
        
        if( ($toprestaurants = Cache::read('toprestaurants')) === false ){
            $toprestaurants = $this->TopRestaurants->find('all',$conditions);
            Cache::write('toprestaurants', $toprestaurants);
        }
        
        if( $toprestaurants && count($toprestaurants) >0 ){
            
            App::import('Helper', 'Text');
            $text = new TextHelper();
            $toprestaurantsData = array();
            mb_internal_encoding('UTF-8');
            
            foreach( $toprestaurants as $toprestaurant ){
                
                $title = trim($toprestaurant['TopRestaurants']['title']);
                $address = trim($toprestaurant['TopRestaurants']['address']);
                $details = $text->truncate(trim($toprestaurant['TopRestaurants']['details']),100000);
                $favourite_dish = $text->truncate(trim($toprestaurant['TopRestaurants']['favourite_dish']),100000);
                $opening_hours = $text->truncate(trim($toprestaurant['TopRestaurants']['opening_hours']),100000);
                $facebook_id = null;
                $facebook_id = ( strlen(trim($toprestaurant['TopRestaurants']['facebook_id']))>0 && 
                                 !is_null($toprestaurant['TopRestaurants']['facebook_id']) ) ? trim($toprestaurant['TopRestaurants']['facebook_id']):'';
                
                $retina = null;
                $non_retina = null;
                $img_src = DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'original'.DIRECTORY_SEPARATOR.'top_restaurants'.DIRECTORY_SEPARATOR.trim($toprestaurant['TopRestaurants']['image']);
                $base_path = APP.'webroot'.$img_src;
                if(is_file($base_path)){
                    $retina = Configure::read('baseUri').DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'original'.DIRECTORY_SEPARATOR.'top_restaurants'.DIRECTORY_SEPARATOR.trim($toprestaurant['TopRestaurants']['image']);
                    $non_retina = Configure::read('baseUri').DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'thumbs'.DIRECTORY_SEPARATOR.'480_320'.DIRECTORY_SEPARATOR.'top_restaurants'.DIRECTORY_SEPARATOR.'thumbs_'.trim($toprestaurant['TopRestaurants']['image']);
                    $toprestaurantsData[] = array(
                        'id'        => intval($toprestaurant['TopRestaurants']['id']),
                        'title'     => $title,// &nbsp; and * is the delimeter options
                        'address'   => $this->ucwords_specific( mb_strtolower($address, 'UTF-8'), " *"),
                        'phone'     => trim($toprestaurant['TopRestaurants']['phone']),
                        'details'   => str_replace("\r\n","\n",$details),
                        'latitude'  => trim($toprestaurant['TopRestaurants']['latitude']),
                        'longitude' => trim($toprestaurant['TopRestaurants']['longitude']),
                        'price_rating' => intval($toprestaurant['TopRestaurants']['price_rating']),
                        'quality_rating'=> intval($toprestaurant['TopRestaurants']['quality_rating']),
                        'favourite_dish'=> $favourite_dish ,
                        'opening_hours' => $opening_hours,
                        'facebook_id'   => $facebook_id,
                        'images' => array(
                            'retina' => array(
                                'width'  => 640,
                                'height' => 960,
                                'url'    => $retina
                            ),
                            'non_retina' => array(
                                'width'  => 320,
                                'height' => 480,
                                'url'    => $non_retina
                            )
                        )
                    ); 
                }else{
                    $toprestaurantsData[] = array(
                        'id'        => intval($toprestaurant['TopRestaurants']['id']),
                        'title'     => $title,
                        'address'   => $this->ucwords_specific( mb_strtolower($address, 'UTF-8'), " *"),
                        'phone'     => trim($toprestaurant['TopRestaurants']['phone']),
                        'details'   => str_replace("\r\n","\n",$details),
                        'latitude'  => trim($toprestaurant['TopRestaurants']['latitude']),
                        'longitude' => trim($toprestaurant['TopRestaurants']['longitude']),
                        'price_rating' => intval($toprestaurant['TopRestaurants']['price_rating']),
                        'quality_rating'=> intval($toprestaurant['TopRestaurants']['quality_rating']),
                        'favourite_dish'=> $favourite_dish ,
                        'opening_hours' => $opening_hours,
                        'facebook_id' => $facebook_id,
                        'images' => null
                    );
                    
                }
                unset($toprestaurant);
            }
            
            $content_for_layout = array(
                'status' => 'Success', 
                'result' => array(
                    'total' => count($toprestaurantsData),
                    'TopRestaurants'=> $toprestaurantsData 
                )
            );
            $this->set(compact('content_for_layout'));
            
        }else{
            $this->throwExceptionError($errorCode=412, $errorMessage =$this->__httpCodes[412]);
        }
        
        $args['requestRS'] = $this->render();
        $this->requestLogsUpdate( $args , $transactionIdentifier );
    }

    
    public function CheapEats(){
        Configure::write('debug',0);
        $transactionIdentifier = $this->requestLogs(array('requestMethod' => __FUNCTION__));
        $conditions = array(
            'conditions' => array('CheapEats.status' => Configure::read("status_live") ),   
            'fields' => array(
                'CheapEats.id',
                'CheapEats.title',
                'CheapEats.address',
                'CheapEats.phone',
                'CheapEats.details',
                'CheapEats.style_of_food',
                'CheapEats.image',
                'CheapEats.latitude',
                'CheapEats.longitude',
                'CheapEats.facebook_id'
            ),
            'order' => array('CheapEats.title ASC')
        );
        
        if( ($cheapeats = Cache::read('cheapeats')) === false ){
            $cheapeats = $this->CheapEats->find('all',$conditions);
            Cache::write('cheapeats', $cheapeats);
        }
        
        if( $cheapeats && count($cheapeats) >0 ){
            
            App::import('Helper', 'Text');
            $text = new TextHelper();
            $cheapeatData = array();
            mb_internal_encoding('UTF-8');
            
            foreach( $cheapeats as $cheapeat ){
                
                $title = trim($cheapeat['CheapEats']['title']);
                $address = trim($cheapeat['CheapEats']['address']);
                $details = $text->truncate(trim($cheapeat['CheapEats']['details']),100000);
                $style_of_food = $text->truncate(trim($cheapeat['CheapEats']['style_of_food']),100000);
                
                $facebook_id = null;
                $facebook_id = ( strlen(trim($cheapeat['CheapEats']['facebook_id']))>0 && 
                                 !is_null($cheapeat['CheapEats']['facebook_id']) ) ? trim($cheapeat['CheapEats']['facebook_id']):'';
                
                $retina = null;
                $non_retina = null;
                $img_src = DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'original'.DIRECTORY_SEPARATOR.'cheap_eats'.DIRECTORY_SEPARATOR.trim($cheapeat['CheapEats']['image']);
                $base_path = APP.'webroot'.$img_src;
                if(is_file($base_path)){
                    $retina = Configure::read('baseUri').DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'original'.DIRECTORY_SEPARATOR.'cheap_eats'.DIRECTORY_SEPARATOR.trim($cheapeat['CheapEats']['image']);
                    $non_retina = Configure::read('baseUri').DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'thumbs'.DIRECTORY_SEPARATOR.'480_320'.DIRECTORY_SEPARATOR.'cheap_eats'.DIRECTORY_SEPARATOR.'thumbs_'.trim($cheapeat['CheapEats']['image']);
                    
                    $cheapeatData[] = array(
                        'id'        => intval($cheapeat['CheapEats']['id']),
                        'title'     => $title,
                        'address'   => $this->ucwords_specific( mb_strtolower($address, 'UTF-8'), " *"),
                        'phone'     => trim($cheapeat['CheapEats']['phone']),
                        'details'   => str_replace("\r\n","\n",$details),
                        'style_of_food'   => $style_of_food,
                        'latitude'  => trim($cheapeat['CheapEats']['latitude']),
                        'longitude' => trim($cheapeat['CheapEats']['longitude']),
                        'facebook_id' => $facebook_id,
                        'images' => array(
                            'retina' => array(
                                'width'  => 640,
                                'height' => 960,
                                'url'    => $retina
                            ),
                            'non_retina' => array(
                                'width'  => 320,
                                'height' => 480,
                                'url'    => $non_retina
                            )
                        )

                    ); 
                }else{
                    $cheapeatData[] = array(
                        'id'        => intval($cheapeat['CheapEats']['id']),
                        'title'     => $title,
                        'address'   => $this->ucwords_specific( mb_strtolower($address, 'UTF-8'), " *"),
                        'details'   => str_replace("\r\n","\n",$details),
                        'phone'     => trim($cheapeat['CheapEats']['phone']),
                        'style_of_food'   => $style_of_food,
                        'latitude'  => trim($cheapeat['CheapEats']['latitude']),
                        'longitude' => trim($cheapeat['CheapEats']['longitude']),
                        'facebook_id' => $facebook_id,
                        'images'    => null
                    );
                }
                unset($cheapeat);
            }
            
            $content_for_layout = array(
                'status' => 'Success', 
                'result' => array(
                    'total' => count($cheapeatData),
                    'CheapEats'=> $cheapeatData 
                )
            );
            $this->set(compact('content_for_layout'));
            
        }else{
            $this->throwExceptionError($errorCode=412, $errorMessage =$this->__httpCodes[412]);
        }
        
        $args['requestRS'] = $this->render();
        $this->requestLogsUpdate( $args , $transactionIdentifier );
    }
    
    public function SteinlagerBars(){
        Configure::write('debug',0);
        $transactionIdentifier = $this->requestLogs(array('requestMethod' => __FUNCTION__));
        $conditions = array(
            'conditions' => array('SteinlagerBars.status' => Configure::read("status_live") ),   
            'fields' => array(
                'SteinlagerBars.id',
                'SteinlagerBars.title',
                'SteinlagerBars.address',
                'SteinlagerBars.phone',
                'SteinlagerBars.details',
                'SteinlagerBars.image',
                'SteinlagerBars.is_pure_bar',
                'SteinlagerBars.latitude',
                'SteinlagerBars.longitude',
                'SteinlagerBars.facebook_id'
            ),
            'order' => array('SteinlagerBars.title ASC')
        );
        
        if( ($steinlagers = Cache::read('steinlagers')) === false ){
            $steinlagers = $this->SteinlagerBars->find('all',$conditions);
            Cache::write('steinlagers', $steinlagers);
        }
        
        if( $steinlagers && count($steinlagers) >0 ){
            
            App::import('Helper', 'Text');
            $text = new TextHelper();
            $steinlagerData = array();
            mb_internal_encoding('UTF-8');
            
            foreach( $steinlagers as $steinlager ){
                
                $title = trim($steinlager['SteinlagerBars']['title']);
                $address = trim($steinlager['SteinlagerBars']['address']);
                $details = $text->truncate(trim($steinlager['SteinlagerBars']['details']),100000);
                
                $facebook_id = null;
                $facebook_id = ( strlen(trim($steinlager['SteinlagerBars']['facebook_id']))>0 && 
                                 !is_null($steinlager['SteinlagerBars']['facebook_id']) ) ? trim($steinlager['SteinlagerBars']['facebook_id']):'';
                
                $retina = null;
                $non_retina = null;
                $img_src = DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'original'.DIRECTORY_SEPARATOR.'steinlager_bars'.DIRECTORY_SEPARATOR.trim($steinlager['SteinlagerBars']['image']);
                $base_path = APP.'webroot'.$img_src;
                if(is_file($base_path)){
                    $retina = Configure::read('baseUri').DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'original'.DIRECTORY_SEPARATOR.'steinlager_bars'.DIRECTORY_SEPARATOR.trim($steinlager['SteinlagerBars']['image']);
                    $non_retina = Configure::read('baseUri').DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'thumbs'.DIRECTORY_SEPARATOR.'480_320'.DIRECTORY_SEPARATOR.'steinlager_bars'.DIRECTORY_SEPARATOR.'thumbs_'.trim($steinlager['SteinlagerBars']['image']);
                    
                    $steinlagerData[] = array(
                        'id'        => intval($steinlager['SteinlagerBars']['id']),
                        'title'     => $title,
                        'address'   => $this->ucwords_specific( mb_strtolower($address, 'UTF-8'), " *"),
                        'phone'     => trim($steinlager['SteinlagerBars']['phone']),
                        'details'   => str_replace("\r\n","\n",$details),
                        'is_pure_bar' => intval($steinlager['SteinlagerBars']['is_pure_bar']),
                        'latitude'  => trim($steinlager['SteinlagerBars']['latitude']),
                        'longitude' => trim($steinlager['SteinlagerBars']['longitude']),
                        'facebook_id' => $facebook_id,
                        'images' => array(
                            'retina' => array(
                                'width'  => 640,
                                'height' => 960,
                                'url'    => $retina
                            ),
                            'non_retina' => array(
                                'width'  => 320,
                                'height' => 480,
                                'url'    => $non_retina
                            )
                        )
                    ); 
                    
                }else{
                    $steinlagerData[] = array(
                        'id'          => intval($steinlager['SteinlagerBars']['id']),
                        'title'       => $title,
                        'address'     => $this->ucwords_specific( mb_strtolower($address, 'UTF-8'), " *"),
                        'phone'       => trim($steinlager['SteinlagerBars']['phone']),
                        'details'     => str_replace("\r\n","\n",$details),
                        'is_pure_bar' => intval($steinlager['SteinlagerBars']['is_pure_bar']),
                        'latitude'    => trim($steinlager['SteinlagerBars']['latitude']),
                        'longitude'   => trim($steinlager['SteinlagerBars']['longitude']),
                        'facebook_id' => $facebook_id,
                        'images'      => null
                    ); 
                }
                
                
                unset($steinlager);
            }
            
            $content_for_layout = array(
                'status' => 'Success', 
                'result' => array(
                    'total' => count($steinlagerData),
                    'SteinlagerBars'=> $steinlagerData 
                )
            );
            $this->header('HTTP/1.1 200 '.$this->__httpCodes[200]); 
            $this->set(compact('content_for_layout'));
            
        }else{
            $this->throwExceptionError($errorCode=412, $errorMessage =$this->__httpCodes[412]);
        }
        
        $args['requestRS'] = $this->render();
        $this->requestLogsUpdate( $args , $transactionIdentifier );
    }

    private function throwExceptionError($errorCode=0, $errorMessage = null, $errorMethod=''){
        $this->header('HTTP/1.1 '.$errorCode.' '.$this->__httpCodes[$errorCode]);
        $content_for_layout[] = array(
            'status' =>'Failed', 
            'error_details' => array(
                'errorMessage'=> $errorMessage,
                'errorCode'=> $errorCode
            )
        );
        echo json_encode($content_for_layout);
        die;
    }
}