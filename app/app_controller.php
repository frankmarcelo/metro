<?php
class AppController extends Controller {
    
    public $components = array(
        'Security',
        'Acl',
    	'Auth',
        'Session',
        'RequestHandler',
        'Cookie',
    );

    public $helpers = array(
    	'Cache',
        'Html',
        'Form',
        'Session',
        'Text',
        'Js',
        'Time',
        'Layout',
        'Custom',
        'Ajax',
        'Javascript',
        'Crumb'
    );
    
    public $usePaginationCache = true;
    public function __construct() {
        parent::__construct();
    }

    public function beforeFilter() {
        $this->Security->validatePost = true;
        $this->Security->disabledFields = array(
            'TopRestaurants.title',
            'TopRestaurants.address',
            'TopRestaurants.phone',
            'TopRestaurants.details',
            'TopRestaurants.body',
            'TopRestaurants.image',
            'TopRestaurants.quality_rating',
            'TopRestaurants.price_rating',
            'TopRestaurants.favourite_dish',
            'TopRestaurants.opening_hours',
            'TopRestaurants.latitude',
            'TopRestaurants.longitude',
            'CheapEats.title',
            'CheapEats.address',
            'CheapEats.details',
            'CheapEats.style_of_food',
            'CheapEats.image',
            'CheapEats.latitude',
            'CheapEats.longitude',
            'SteinlagerBars.title',
            'SteinlagerBars.address',
            'SteinlagerBars.phone',
            'SteinlagerBars.details',
            'SteinlagerBars.image',
            'SteinlagerBars.is_pure_bar',
            'SteinlagerBars.latitude',
            'SteinlagerBars.longitude'
        );
        if( $this->RequestHandler->isPost() ){
            $this->Security->blackHoleCallback = 'forceSSL';
        }
        
        if ($this->RequestHandler->ext == 'json'|| $this->RequestHandler->isAjax()){ // 'action' ajax requests and all 'action.json' requests receive JSON
            $this->RequestHandler->respondAs('json');
            $this->RequestHandler->setContent('json','application/json');  
        }
        
        
        $this->Auth->loginAction = array('admin'=> false, 'controller' => 'auth', 'action' => 'login');
        $this->Auth->logoutRedirect = array('admin'=> false, 'controller' => 'auth', 'action' => 'logout');
        $this->set('title_for_layout','Metro Mobile App CMS');
    }
    
    public function forceSSL(){
        
    }
}