<?php

App::import('Vendor', 'UrlCache.url_cache_app_helper');

class AppHelper extends UrlCacheAppHelper {

/**
 * Url helper function
 *
 * @param string $url 
 * @param bool $full 
 * @return mixed
 * @access public
 */
    public function url($url = null, $full = false) {
        if (!isset($url['locale']) && isset($this->params['locale'])) {
            $url['locale'] = $this->params['locale'];
        }
        return parent::url($url, $full);
    }
}