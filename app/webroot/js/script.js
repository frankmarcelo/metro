/* 
 * Author:
 * Developer:  Franklin Marcelo
 * Date: 6th of September 2012
*/
Array.prototype.remove = function(index) {
    this.splice(index, 1);
}

var MAPFILES_URL = "http://maps.gstatic.com/intl/en_us/mapfiles/";
var map;
var markersArray = [];
var myLatlng = new google.maps.LatLng(-36.8484597,174.7633315);
var types = [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE];
var geocoder = new google.maps.Geocoder();
var bounds = new google.maps.LatLngBounds();
var geocoder = null;
var shadow = null;
var clickIcon = null;
var clickMarker = null;
var markers = null;
var selected = null;
var infowindow = null;
var boundsOverlay = null;
var viewportOverlay = null;
var initialized = false;
var hashFragment = "";
var results    = null;
var is_edit    = false;
var update_uri = '';

var mapOptions = {
    zoom: 12,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    navigationControl: true,
    mapTypeControl: false,
    disableDefaultUI: true,
    navigationControl: true,
    mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        position: google.maps.ControlPosition.TOP_LEFT
    },
    streetViewControl: false
};



var GeocoderStatusDescription = {
    "OK": "The request did not encounter any errors",
    "UNKNOWN_ERROR": "A geocoding or directions request could not be successfully processed, yet the exact reason for the failure is not known",
    "OVER_QUERY_LIMIT": "The webpage has gone over the requests limit in too short a period of time",
    "REQUEST_DENIED": "The webpage is not allowed to use the geocoder for some reason",
    "INVALID_REQUEST": "This request was invalid",
    "ZERO_RESULTS": "The request did not encounter any errors but returns zero results",
    "ERROR": "There was a problem contacting the Google servers"
};

var GeocoderLocationTypeDescription = {
    "ROOFTOP": "The returned result reflects a precise geocode.",
    "RANGE_INTERPOLATED": "The returned result reflects an approximation (usually on a road) interpolated between two precise points (such as intersections). Interpolated results are generally returned when rooftop geocodes are unavilable for a street address.",
    "GEOMETRIC_CENTER": "The returned result is the geometric center of a result such a line (e.g. street) or polygon (region).",
    "APPROXIMATE": "The returned result is approximate."
}

function toggleCheckboxes(){
    var $mainCheckbox = $('#checkAll'),
    $boxes = $('tbody [type="checkbox"]', $mainCheckbox.closest('table'));
    if ($mainCheckbox.is(':checked')){
        $boxes.attr('checked', 'checked');
    }else{
        $boxes.removeAttr('checked');
    }
}



$(document).ready(function(){
    $("#inputAddress").autocomplete({
        source: function(request, response) {
            if (geocoder == null){
                geocoder = new google.maps.Geocoder();
            }

            geocoder.geocode( {'address': request.term,'country': 'NZ'}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var searchLoc = results[0].geometry.location;
                    var lat = results[0].geometry.location.lat();
                    var lng = results[0].geometry.location.lng();
                    var latlng = new google.maps.LatLng(lat, lng);
                    var bounds = results[0].geometry.bounds;

                    geocoder.geocode({
                        'latLng': latlng
                    }, function(results1, status1) {
                        if (status1 == google.maps.GeocoderStatus.OK) {
                            if (results1[1]) {
                                response($.map(results1, function(loc) {
                                    return {
                                        label  : loc.formatted_address,
                                        value  : loc.formatted_address,
                                        bounds : loc.geometry.bounds,
                                        lat    : lat,
                                        lng    : lng
                                    }
                                }));
                            }
                        }
                    });//end of geocoder.geocode
                }
            });
        },
        select: function(event,ui){
            var pos = ui.item.position;
            var lct = ui.item.locType;
            document.getElementById('latitude').value = ui.item.lat;
            document.getElementById('longitude').value = ui.item.lng;
        }
    });
});

function initialize() {
    var params = parseUrlParams();
    
    clearOptions();
    setOptions(params);
    
    map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
    geocoder = new google.maps.Geocoder();
    infowindow = new google.maps.InfoWindow({
        'size': new google.maps.Size(120, 70)
    });
    
    shadow = new google.maps.MarkerImage(
        MAPFILES_URL + "shadow50.png",
        new google.maps.Size(37, 34),
        new google.maps.Point(0, 0),
        new google.maps.Point(10, 34)
    );

    clickIcon = new google.maps.MarkerImage(
        MAPFILES_URL + 'dd-start.png',
        new google.maps.Size(20, 34),
        new google.maps.Point(0, 0),
        new google.maps.Point(10, 34)
    );

    google.maps.event.addListener(map, 'idle', function() {
        if (document.getElementById("inputAddress").value && ! initialized) {
            if( is_edit == true ){
                overRideQuery();
            }else{
                submitQuery();
            }
        }
        initialized = true;
    });
    
    document.getElementById('inputAddress').onkeyup = function(e) {
        if (!e) var e = window.event;
        if (e.keyCode != 13) return;
        document.getElementById("inputAddress").blur();
        submitQuery();
    }
}

function onClickCallback(event){
    document.getElementById("inputAddress").value = event.latLng.toUrlValue(6);
    geocode({'latLng': event.latLng});
}

function parseUrlParams() {
    var params = {};
  
    if (window.location.search) {
        params.query = unescape(window.location.search.substring(1));
    }
  
    if (window.location.hash) {
        hashFragment = unescape(window.location.hash);
        var args = hashFragment.substring(1).split('&');
        for (var i in args) {
            var param = args[i].split('=');
            switch (param[0]) {
                case 'q':
                    params.query = unescape(param[1]);
                    break;
            }
        }
    }
  
    return params;
}

function clearOptions() {
}

function setOptions(params) {
    if (params.query) {
        document.getElementById("inputAddress").value = params.query;
    }
}

function submitQuery() {
    var query = document.getElementById("inputAddress").value;
    geocode({'address': query});
}

function overRideQuery(){
    params = parseUrlParams();
    clearOptions();
    setOptions(params);
    resetMap();

    var resultsListHtml = '<div class="infoWindowContent"><table class="tabContent"><tr><td><div>'+$('#inputAddress').val()+'</div></td></tr></table></div>';
    
    var openInfoWindow = function(marker) {
        return function() {
            infowindow.setContent(resultsListHtml);
            infowindow.open(map, marker);
        }
    }

    var dragEndLatLng = function(marker) {
        return function() {
            var location = marker.getPosition();
            $.get(update_uri, { 
                latitude: location.lat(), 
                longitude: location.lng(),
                id: $('#id').val()
            });
            $('#latitude').val(location.lat());
            $('#longitude').val(location.lng());
        }
    };

    var latlng = new google.maps.LatLng($("#latitude").val(),$("#longitude").val());
    var i = 0;
    var icon = new google.maps.MarkerImage(
        getMarkerImageUrl(i),
        new google.maps.Size(20, 34),
        new google.maps.Point(0, 0),
        new google.maps.Point(10, 34)
    );
    
    markers[i] = new google.maps.Marker({
        position: latlng,
        map: map,
        icon: icon,
        shadow: shadow,
        draggable: true
    });

    map.setCenter(latlng);
    map.setZoom(14);
    google.maps.event.addListener(markers[i], 'click', openInfoWindow(markers[i]));
    google.maps.event.addListener(markers[i], 'dragend', dragEndLatLng(markers[i]));
    google.maps.event.trigger(markers[i], 'click');
}

function urlencode (str) {
    str = (str + '').toString();
    return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
    replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
}

function geocode(request) {  
    resetMap();
    var hash = '';
    hash = 'q=' + request.address;
    var country = 'NZ';
    //hash += '&country=' + country;
    request.country = country;
    geocoder.geocode(request, showResults);
}

function parseLatLng(value) {
    value.replace('/\s//g');
    var coords = value.split(',');
    var lat = parseFloat(coords[0]);
    var lng = parseFloat(coords[1]);
    if (isNaN(lat) || isNaN(lng)) {
        return null;
    } else {
        return new google.maps.LatLng(lat, lng);
    }
}

function resetMap() {
    infowindow.close();
    
    if( typeof(clickMarker)!='object' ){
        clickMarker.setMap(null);
        clickMarker = null;
    }
    
    map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
    markers = [];
    selected = null;
    clearBoundsOverlays();
  
    document.getElementById("responseCount").style.display = "none";
    document.getElementById("matches").style.display = "none";
}

function showResults(results, status) {
    
    var reverse = (clickMarker != null);
  
    if (! results) {
        alert("Geocoder did not return a valid response");
    } else {
        document.getElementById("statusValue").innerHTML = status;
        document.getElementById("statusDescription").innerHTML = GeocoderStatusDescription[status];

        document.getElementById("responseInfo").style.display = "block";    
        document.getElementById("responseStatus").style.display = "block";

        if (status == google.maps.GeocoderStatus.OK) {
            
            map.setCenter(results[0].geometry.location);
            map.setZoom(14);
            $('#latitude').val(results[0].geometry.location.lat());
            $('#longitude').val(results[0].geometry.location.lng());
            document.getElementById("matchCount").innerHTML = results.length;       
            document.getElementById("responseCount").style.display = "block";
            plotMatchesOnMap(results, reverse);
        } 
    }
}

function plotMatchesOnMap(results, reverse) {
  
    markers = new Array(results.length);
    var resultsListHtml = "";
    var openInfoWindow = function(resultNum, result, marker) {
        return function() {
            if (selected != null) {
                document.getElementById('p' + selected).style.backgroundColor = "white";
                clearBoundsOverlays();
            }
      
            map.fitBounds(result.geometry.viewport);
            infowindow.setContent(getAddressComponentsHtml(result.address_components));
            infowindow.open(map, marker);
        }
    }
    
    var dragEndLatLng = function(resultNum, result, marker) {
        return function() {
            var location = marker.getPosition();
            $('#latitude').val(location.lat());
            $('#longitude').val(location.lng());
        }
    };
    
    for (var i = 0; i < results.length; i++) {
        var icon = new google.maps.MarkerImage(
            getMarkerImageUrl(i),
            new google.maps.Size(20, 34),
            new google.maps.Point(0, 0),
            new google.maps.Point(10, 34)
        );
    
        markers[i] = new google.maps.Marker({
            position: results[i].geometry.location,
            map: map,
            icon: icon,
            shadow: shadow,
            draggable: true
        });
        google.maps.event.addListener(markers[i], 'click', openInfoWindow(i, results[i], markers[i]));
        google.maps.event.addListener(markers[i], 'dragend', dragEndLatLng(i, results[i], markers[i]));
        resultsListHtml += getResultsListItem(i, getResultDescription(results[i]));
    }
    
    document.getElementById("matches").innerHTML = resultsListHtml;
    document.getElementById("p0").style.border = "none";
    document.getElementById("matches").style.display = "block";
    selectMarker(0);
}

function selectMarker(n) {
    google.maps.event.trigger(markers[n], 'click');
}

function zoomToViewports(results) {
    var bounds = new google.maps.LatLngBounds();

    for (var i in results) {
        bounds.union(results[i].geometry.viewport);
    }

    map.fitBounds(bounds);
}

function getMarkerImageUrl(resultNum) {
    return MAPFILES_URL + "marker" + String.fromCharCode(65 + resultNum) + ".png";
}

function getResultsListItem(resultNum, resultDescription) {
    var html  = '<a onclick="selectMarker(' + resultNum + ')">';
    html += '<div class="info" id="p' + resultNum + '">';
    html += '<table><tr valign="top">';
    html += '<td style="padding: 2px"><img src="' + getMarkerImageUrl(resultNum) + '"/></td>';
    html += '<td style="padding: 2px">' + resultDescription + '</td>';
    html += '</tr></table>';
    html += '</div></a>';
    return html;
}

function getResultDescription(result) {
    var bounds = result.geometry.bounds;
    var html  = '<table class="tabContent">';
    html += tr('Address', result.formatted_address);
    html += tr('Types', result.types.join(", "));
    html += tr('Location', result.geometry.location.toString());
    html += tr('Bounds', (bounds ? boundsToHtml(bounds) : "None"));
    html += tr('Viewport', boundsToHtml(result.geometry.viewport));
    html += tr('Location type', result.geometry.location_type);
    if (result.partial_match) {
        html += tr('Partial match', 'Yes');
    }
    html += '</table>';
    return html;
}

function getAddressComponentsHtml(components) {
    var html = '<div class="infoWindowContent">' +
    '<table class="tabContent"><tr><td>';
    var html_info =''; 
    for (var i = 0; i < components.length; i++) {   
        if ( components[i].long_name && components[i].long_name !=null && components[i].long_name !='null' ){
            html_info += components[i].long_name+ " ";
        }
    }
    
    html += '<div>'+html_info+'</div>';
    html += '</td></tr></table></div>';
    return html;
}

function tr(key, value) {
    return '<tr>' +
    '<td class="key">' + key + (key ? ':' : '') + '</td>' +
    '<td class="value">' + value + '</td>' +
    '</tr>';
}

function br() {
    return '<tr><td colspan="2"><div style="width: 100%; border-bottom: 1px solid grey; margin: 2px;"</td></tr>';
}

function clearBoundsOverlays() {
   
}

function boundsToHtml(bounds) {
    return '(' +
    bounds.getSouthWest().toUrlValue(6) +
    ') -<br/>(' +
    bounds.getNorthEast().toUrlValue(6) +
    ')';
}
