<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas
 **/
return array(
     'js' => array(
         '//js/bootstrap/bootstrap-transition.js',
         '//js/bootstrap/bootstrap-alert.js',
         '//js/bootstrap/bootstrap-modal.js',
         '//js/bootstrap/bootstrap-dropdown.js',
         '//js/bootstrap/bootstrap-scrollspy.js',
         '//js/bootstrap/bootstrap-tab.js',
         '//js/bootstrap/bootstrap-tooltip.js',
         '//js/bootstrap/bootstrap-popover.js',
         '//js/bootstrap/bootstrap-button.js',
         '//js/bootstrap/bootstrap-collapse.js',
         '//js/bootstrap/bootstrap-carousel.js',
         '//js/bootstrap/bootstrap-typeahead.js',
         '//js/libs/jquery.maskmoney.js',
         '//js/plugins.js', 
         '//js/script.js',
     ),
     'css' => array(
         '//css/main.css',
         '//css/normalize.css',
         '//css/bootstrap-responsive.css'
     ),
);