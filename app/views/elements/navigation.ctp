<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a>
            <a class="brand" href="/"><?php echo Configure::read('Application.Name');?></a>
            <?php
            if( intval($this->Session->read('Auth.User.id')) > 0 ){
            ?>
            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li<?php echo in_array(strtolower($this->name),array('topfifty')) ?' class="active"':'';?>><?php echo str_replace(array("_","fifty"),array("-","50"),$this->Html->link(__('Top 50 Restaurants',true),array('controller'=>'top_fifty', 'action' => 'index')));?></li>
                    <li<?php echo in_array(strtolower($this->name),array('cheapeats')) ?' class="active"':'';?>><?php echo str_replace("_","-",$this->Html->link(__('Cheap Eats',true),array('controller'=>'cheap_eats', 'action' => 'index')));?></li>
                    <li<?php echo in_array(strtolower($this->name),array('steinlagerbars')) ?' class="active"':'';?>><?php echo str_replace("_","-",$this->Html->link(__('Steinlager Bars',true),array('controller'=>'steinlager_bars', 'action' => 'index')));?></li>
                </ul>
            </div><!--/.nav-collapse -->
            <a href="<?php echo $this->Html->url(array("controller" => "auth","action" => "logout"));?>" class="btn btn-inverse span1 offset3"><i class="icon-off icon-white"></i>Log out</a>
            <?php
            }
            ?>
        </div>
    </div>
</div>