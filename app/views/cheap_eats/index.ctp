<?php echo $this->element('navigation');?>
<div class="clear:both;"></div><br><br>
<style type="text/css">
p{
    font-size: 13px !important;
}
.anchor{
    font-size: 16px !important;
}
</style>      
<div class="container-fluid">
    <div class="container-fluid">
        <div class="page-header">
            <?php 
            if($cheapeats && false){?><div class="pull-right"><form class="form-search" action="<?php echo $this->Html->url(array("action" => "search"));?>"><input type="text" name="q" id="q" class="span3 search-query" ><button type="submit" class="btn">Search</button></form></div><?php }?>
            <h1>Cheap Eats<?php if($cheapeats){?><br><small class="span12 offset5 pull-center"><?php echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));?></small><?php }?></h1>
        </div>
        <?php
        $this->Layout->sessionFlash();
        ?>
        <div class="row-fluid">
            <div class="span12 container-fluid">  
                <?php
                if( !empty($cheapeats) && sizeof($cheapeats)>0 ){
                ?>
                    <?php
                    echo str_replace('<form','<form class="form-horizontal"',$this->Form->create('CheapEats', array(
                            'url' => array('controller' => 'cheap_eats', 'action' => 'batch_delete')))
                    );
                    ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th style="width:20px;"><div><p><strong><input id="checkAll" type="checkbox" onclick="toggleCheckboxes();" /></strong></p></div></th>
                                <th style="width:100px;"><div><p><strong><?php echo $paginator->sort('Title','title');?></strong></p></div></th>
                                <th style="width:150px;"><div><p><strong><?php echo $paginator->sort('Address','address');?></strong></p></div></th>
                                <th style="width:250px;"><div><p><strong>Details</strong></p></div></th>
                                <th style="width:250px;"><div><p><strong>Style of Food</strong></p></div></th>
                                <th style="width:100px;"><div><p><strong>Image</strong></p></div></th>
                                <th style="width:100px;"><div><p><strong><?php echo $paginator->sort('Created At','created_at');?></strong></p></div></th>
                                <th style="width:100px;"><div><p><strong>Actions</strong></p></div></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach( $cheapeats as $cheapeat ){
                                $thumbsrc = null;
                                $img_src = DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'original'.DIRECTORY_SEPARATOR.'cheap_eats'.DIRECTORY_SEPARATOR.trim($cheapeat['CheapEats']['image']);
                                $base_path = APP.'webroot'.$img_src;
                                if(is_file($base_path)){
                                    $thumbsrc = DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'thumbs'.DIRECTORY_SEPARATOR.'480_320'.DIRECTORY_SEPARATOR.'cheap_eats'.DIRECTORY_SEPARATOR.'thumbs_'.trim($cheapeat['CheapEats']['image']);
                                }
                            
                                $editUri = $this->Html->url(array("controller"=>"cheap_eats","action"=>'edit',intval($cheapeat['CheapEats']['id']).DIRECTORY_SEPARATOR.convertToSeoUri(trim($cheapeat['CheapEats']['title']))));
                                $deleteUri = $this->Html->url(array("controller"=>"cheap_eats","action"=>'delete',intval($cheapeat['CheapEats']['id'])));
                            ?>
                            <tr>
                                <td><input type="checkbox" name="data[CheapEats][id][]" value="<?php echo intval($cheapeat['CheapEats']['id']);?>" /></td>
                                <td><div><p><strong><a href="<?php echo $editUri;?>"><?php echo trim($cheapeat['CheapEats']['title']);?></a></strong></p></div></td>
                                <td><div><p><?php echo trim($cheapeat['CheapEats']['address']);?></p></div></td>
                                <td><div><p><?php echo $this->Text->truncate(trim($cheapeat['CheapEats']['details']),150);?></p></div></td>
                                <td><div><p><?php echo $this->Text->truncate(trim($cheapeat['CheapEats']['style_of_food']),250);?></p></div></td>
                                <td><div><p><?php if($thumbsrc){?><img src="<?php echo $thumbsrc;?>" height="45px" width="45px" /><?php }?></p></div></td>
                                <td><div><p><?php echo date("D j",strtotime(trim($cheapeat['CheapEats']['created_at']))).'<sup>'.date("S",strtotime(trim($cheapeat['CheapEats']['created_at']))).'</sup>&nbsp;'.date("F",strtotime(trim($cheapeat['CheapEats']['created_at'])));?></p></div></td>
                                <td style="text-align:right;">
                                    <div class="btn-group"><a class="btn" href="<?php echo $editUri;?>">Edit</a>    
                                    <a class="btn btn-danger" onclick="
                                        if (confirm('Are you sure?')) { 
                                            var f = document.createElement('form'); 
                                            f.style.display = 'none'; 
                                            this.parentNode.appendChild(f); 
                                            f.method = 'post'; 
                                            f.action = this.href;
                                            var m = document.createElement('input'); 
                                            m.setAttribute('type', 'hidden');
                                            m.setAttribute('name', 'sf_method'); 
                                            m.setAttribute('value', 'delete'); 
                                            f.appendChild(m);f.submit(); };return false;" href="<?php echo $deleteUri;?>">Delete</a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="10">
                                    <div class="pull-right">
                                        <div class="pagination">
                                            <ul>
                                                <?php
                                                //echo $paginator->first('<< First',array('model'=> 'CheapEats','tag'=>'li','class'=>'')).' '.
						//$paginator->prev('< Previous',array('model'=> 'CheapEats','tag'=>'li','class'=>'')).' '.
						echo $paginator->numbers(array('model'=> 'CheapEats','tag'=>'li','class'=>'')).' ';
						//$paginator->next('Next >',array('model'=> 'CheapEats','tag'=>'li','class'=>'')).' '.
						//$paginator->last('Last >>',array('model'=> 'CheapEats','tag'=>'li','class'=>''));
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="btn-toolbar">
                                        <div class="btn-group"><a id="dropdown_toggle" class="btn dropdown-toggle" data-toggle="dropdown" href="#">Choose an action    <span class="caret"></span></a>
                                        <ul class="batch-actions dropdown-menu"><li><a href="#" data-action="batchDelete">Delete</a></li></ul></div>
                                        <input type="hidden" name="batch_action" value="" />
                                        <script type="text/javascript">
                                        $('.batch-actions a').bind('click', function(){
                                            var $anchor = $(this),
                                            $form = $anchor.closest('form');
                                            $('input[name="batch_action"]', $form).val($anchor.data('action'));
                                            $form.submit();
                                            return false;
                                        });
                                        
                                        $('#dropdown_toggle').dropdown();
                                        </script>
                                        <div class="btn-group"><a href="<?php echo $this->Html->url(array("controller" => "cheap_eats","action" => "add"));?>" class="btn btn-info"><i class="icon-plus icon-white"></i> New</a></div>
                                    </div>
                                </th>
                            </tr>
                        </tfoot>
                    </table>  
                    <?php echo $this->Form->end();?>
                 </form>     
                <?php
                }else{
                ?>
                <p><a class="btn btn-primary btn-large" href="<?php echo $this->Html->url(array("controller" => "cheap_eats","action" => "add"));?>" id="addtop50" role="button" data-toggle="modal">Add Cheap Eats</a></p>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>