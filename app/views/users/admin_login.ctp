<?php echo $this->element('navigation');?>
<div class="clear:both;"></div><br><br><br>
<div class="container">
<?php
$sessionMessage = $this->Session->flash('auth');
?>
    <script type="text/javascript">
    //<![CDATA[
    <?php
    if( is_null($sessionMessage) || $sessionMessage == false ){
    $sessionFlash = $this->Session->read('Message.flash.message');
    $this->Session->delete('Message.flash');
    $this->Session->delete('Auth.redirect');
    }else{
    $sessionFlash = $sessionMessage;
    $this->Session->delete('Message.flash');
    $this->Session->delete('Auth.redirect');
    }

    if( $sessionFlash != false ){
    ?>
    addLoadEvent = function(func){if(typeof jQuery!="undefined")jQuery(document).ready(func);else if(typeof wpOnload!='function'){wpOnload=func;}else{var oldonload=wpOnload;wpOnload=function(){oldonload();func();}}};
    function s(id,pos){g(id).left=pos+'px';}
    function g(id){return document.getElementById(id).style;}
    function shake(id,a,d){c=a.shift();s(id,c);if(a.length>0){setTimeout(function(){shake(id,a,d);},d);}else{try{g(id).position='static';wp_attempt_focus();}catch(e){}}}
    addLoadEvent(function(){ var p=new Array(15,30,15,0,-15,-30,-15,0);p=p.concat(p.concat(p));var i=document.forms[0].id;g(i).position='relative';shake(i,p,20);});
    //]]>
    <?php
    }
    ?>
    </script>
    <div class="clear:both" />
    <div class="container">
    <?php
    if( strlen(trim($sessionFlash)) >0 && !is_null($sessionFlash) ){
        if(strstr($sessionFlash,'class') ){
        echo $sessionFlash;
        }else{
        if( strstr($sessionFlash,'successful') || strstr($sessionFlash,'resetting') ){
            echo '<div class="success"><strong>SUCCESS</strong> : '.$sessionFlash.'</div>';
        }else{  
            echo '<div id="authMessage" class="message"><strong>ERROR</strong> : '.$sessionFlash.'</div>';
        }
        }
    }

    echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'login')));?>
    <?php echo $this->Form->input('username',array('label'=>'Username','class'=>'input','div'=>false)); ?>
    <?php echo $this->Form->input('password',array('label'=>'Password','class'=>'input','div'=>false));?>
    <br/>
    <?php echo $this->Form->end(array('tabindex'=>3,'type'=>'submit','label'=>__('Log In', true),'class' => 'button-primary','name' => 'Submit'	));?>
    </div>
</div>