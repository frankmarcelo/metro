<?php echo $this->element('navigation');?>
<div class="clear:both;"></div><br><br><br>
<div class="container">
<?php
$sessionMessage = $this->Session->flash('auth');
?>
    <script type="text/javascript">
    //<![CDATA[
    <?php
    if( isset($sessionFlash) && $sessionFlash != false ){
    ?>
    addLoadEvent = function(func){if(typeof jQuery!="undefined")jQuery(document).ready(func);else if(typeof wpOnload!='function'){wpOnload=func;}else{var oldonload=wpOnload;wpOnload=function(){oldonload();func();}}};
    function s(id,pos){g(id).left=pos+'px';}
    function g(id){return document.getElementById(id).style;}
    function shake(id,a,d){c=a.shift();s(id,c);if(a.length>0){setTimeout(function(){shake(id,a,d);},d);}else{try{g(id).position='static';wp_attempt_focus();}catch(e){}}}
    addLoadEvent(function(){ var p=new Array(15,30,15,0,-15,-30,-15,0);p=p.concat(p.concat(p));var i=document.forms[0].id;g(i).position='relative';shake(i,p,20);});
    <?php
    }
    ?>
    //]]>
    </script>
    <div class="clear:both" />
    <div class="row span3">
        <?php
        if( isset($sessionFlash) && strlen(trim($sessionFlash)) >0 && !is_null($sessionFlash) ){
            if(strstr($sessionFlash,'class') ){
                echo $sessionFlash;
            }else{
                if( strstr($sessionFlash,'successful') || strstr($sessionFlash,'resetting') ){
                    echo '<div class="success"><strong>SUCCESS</strong> : '.$sessionFlash.'</div>';
                }else{  
                    echo '<div class="control-group span4 offset1"><em><p class="text-error">'.$sessionFlash.'</p></em></div>';
                }
            }
        }

        echo str_replace('form','form class="form-horizontal"',$this->Form->create('User', array('url' => array('controller' => 'auth', 'action' => 'login'))));
        ?>
        <div class="control-group">
            <label class="control-label" for="inputUsername">Username</label>
            <div class="controls"><input type="text" id="inputUsername" name="data[User][username]" maxlength="50" placeholder="Username" /></div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputPassword">Password</label>
            <div class="controls"><input type="password" id="inputPassword" name="data[User][password]" maxlength="50" placeholder="Password" /></div>
        </div>
        <div class="control-group">
            <div class="controls"><input type="submit" class="btn" id="inputSubmit" name="Login" placeholder="Log In" value="Log In" /></div>
        </div>
        <?php echo $this->Form->end();?>
    </div>
</div>