<?php echo $this->element('navigation');?>
<div class="clear:both;"></div><br><br>
<div class="row">
    <div class="span12">
        <div class="page-header">
            <div class="pull-right"><form class="form-search" action="<?php echo $this->Html->url(array("controller" => "top_fifty","action" => "search"));?>"><input type="text" name="q" id="q" class="span3 search-query" ><button type="submit" class="btn">Search</button></form></div>
            <h1>Top 50 Restaurants<small class="offset2">10 results</small></h1>
        </div>
        <div class="row-fluid">
            <div class="span8 container-fluid">  
                <?php
                if( !empty($cheap_eats) && sizeof($cheap_eat)>0 ){
                ?>
                <form class="form-search">
                    <div class="input-append">
                        <input type="text" name="q" id="q" class="span12 search-query" >
                        <button type="submit" class="btn">Search</button>
                    </div>
                </form>
                <table class="table table-striped table-condensed">
                    <thead>
                        <tr>
                        <th>Title</th>
                        <th>Address</th>
                        <th>Phone Number</th>
                        <th>Body Copy</th>
                        <th>Image</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>   
                </table>
                <?php
                }else{
                ?>
                <div class="container">
                    <p><a class="btn btn-primary btn-large" href="<?php echo $this->Html->url(array("controller" => "top_fifty","action" => "add"));?>" id="addtop50" role="button" data-toggle="modal">Add Top 50 Restaurants</a></p>
                    <!--
                    <div style="display:none" class="modal" id="top50" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="myModalLabel">Modal header</h3>
                        </div>
                        <div class="modal-body"><p>One fine body…</p></div>
                        <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                        <button class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                    
                    
                    <a href="#myModal" role="button" class="btn" data-toggle="modal">Launch demo modal</a>
<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<h3 id="myModalLabel">Modal header</h3>
</div>
<div class="modal-body">
<p>One fine body…</p>
</div>
<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
<button class="btn btn-primary">Save changes</button>
</div>
</div>-->
                </div>
                <?php
                }
                ?>
            </div>
            <div class="span4 container-fluid"><p>
                    
            </p></div>
        </div>
    </div>
</div>