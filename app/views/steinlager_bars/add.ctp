<?php echo $this->element('navigation');?>
<div class="clear:both;"></div><br><br>
<style type="text/css">
#map_canvas {
    background-color: #E5E3DF;
    height: 500px;
    overflow: hidden;
    position: relative;
    top: 0 !important;
}
</style>
<div class="container">
    <div class="row">
        <div class="span12">
            <div class="page-header"><h1>Add Steinlager Bar</h1></div>
            <?php
            $this->Layout->sessionFlash();
            
            $this->Html->addCrumb('Home', '/');
            $this->Html->addCrumb('Steinlager Bars', $this->Html->url(array("controller" => "steinlager_bars",'action'=>'index')));
            $this->Html->addCrumb('Add');
            echo $this->element('breadcrumb');
            ?>
            <div class="row-fluid">
                <div class="span8 container-fluid">  
                    <?php
                    echo str_replace('<form','<form class="form-horizontal"',$this->Form->create('SteinlagerBars', array(
                            'url' => array('controller' => 'steinlager_bars', 'action' => 'add'),
                            'type' => 'file'
                        ))
                    );
                    ?>
                    <input type="hidden" name="data[SteinlagerBars][latitude]" id="latitude" value="<?php echo (!isset($steinlagers) && !isset($steinlagers['SteinlagerBars']['latitude']))? Configure::read('GoogleGeocode.Latitude'):$steinlagers['SteinlagerBars']['latitude'];?>" />
                    <input type="hidden" name="data[SteinlagerBars][longitude]" id="longitude" value="<?php echo (!isset($steinlagers) && !isset($steinlagers['SteinlagerBars']['longitude']))? Configure::read('GoogleGeocode.Longitude'):$steinlagers['SteinlagerBars']['longitude'];?>" />
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputTitle">Title</label>
                        <div class="controls"><input type="text" class="input-xlarge" id="inputTitle" value="<?php echo (!isset($steinlagers) && !isset($steinlagers['SteinlagerBars']['title']))? '':trim($steinlagers['SteinlagerBars']['title']);?>" name="data[SteinlagerBars][title]" maxlength="100" placeholder="Title..." /></div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputAddress">Address</label>
                        <div class="controls"><input type="text" class="input-xlarge" id="inputAddress" value="<?php echo (!isset($steinlagers) && !isset($steinlagers['SteinlagerBars']['address']))? '':trim($steinlagers['SteinlagerBars']['address']);?>" name="data[SteinlagerBars][address]" maxlength="100" placeholder="Address..." /><a class="btn" style="cursor:pointer" onclick="submitQuery()">Geocode</a></div>
                    </div>
                    <div class="control-group control-type-text" style="display:none;">
                        <label class="control-label" for="inputPhone">Phone Number</label>
                        <div class="controls"><input type="text" class="input-xlarge" id="inputPhone" value="<?php echo (!isset($steinlagers) && !isset($steinlagers['SteinlagerBars']['phone']))? '':trim($steinlagers['SteinlagerBars']['phone']);?>" name="data[SteinlagerBars][phone]" maxlength="50" placeholder="Phone..." /></div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputInfo">Info</label>
                        <div class="controls"><textarea rows="5" class="input-xlarge" id="inputInfo" name="data[SteinlagerBars][details]" placeholder="Info text...."><?php echo (!isset($steinlagers) && !isset($steinlagers['SteinlagerBars']['details']))? '':trim($steinlagers['SteinlagerBars']['details']);?></textarea></div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputBar">Is Pure Bar</label>
                        <div class="controls">
                            <select class="input-xlarge" name="data[SteinlagerBars][is_pure_bar]" id="inputBar">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputFacebook">Facebook Id</label>
                        <div class="controls"><input type="text" class="input-xlarge" id="inputFacebook" name="data[SteinlagerBars][facebook_id]" value="<?php echo (!isset($steinlagers) && !isset($steinlagers['SteinlagerBars']['facebook_id']))? '':trim($steinlagers['SteinlagerBars']['facebook_id']);?>" maxlength="50" placeholder="Facebook Id..." /></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputImage">Image</label>
                        <div class="controls"><input type="file" class="input-xlarge" id="inputImage" name="data[SteinlagerBars][image]" /></div>
                    </div>
                    <div class="form-actions"><div class="btn-group">
                        <button type="submit" class="btn btn-primary" id="inputSubmit" name="Login" placeholder="Save" value="Save">Save</button>&nbsp;&nbsp;<a class="btn" href="<?php echo $this->Html->url(array("controller"=>"steinlager_bars","action"=>'index'));?>">Back to list</a>
                    </div></div>
                    <?php echo $this->Form->end();?>
                </div>
                <div class="offset1">
                    <div id="map_canvas" style="height:500px;top:300px"></div>
                    <div style="display:none;">
                        <div id="responseInfo">
                            <div id="responseStatus">
                                <div>
                                <span style="font-weight: bold">Geocoder response: </span>
                                <span id="statusValue"></span> (<span id="statusDescription"></span>)
                                </div>
                            </div>
                            <div id="responseCount">
                                <span style="font-weight: bold;">Matches returned: </span>
                                <span id="matchCount"></span>
                            </div>
                        </div>
                        <div id="matches"></div>
                        <div id="boundsLegend">Bounds</div>
                        <div id="viewportLegend">Viewport</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br>
<script type="text/javascript">
/* <![CDATA[ */    
    $(document).ready(function(){
        initialize(); 
        $("#inputPhone").mask("(09) 999-9999");
    });
/* ]]> */
</script>
