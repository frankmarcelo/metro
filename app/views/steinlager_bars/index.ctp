<?php echo $this->element('navigation');?>
<div class="clear:both;"></div><br><br>
<style type="text/css">
p{
    font-size: 13px !important;
}
.anchor{
    font-size: 16px !important;
}
</style>      
<div class="container-fluid">
    <div class="container-fluid">
        <div class="page-header">
            <?php 
            if($steinlagerbars && false){?><div class="pull-right"><form class="form-search" action="<?php echo $this->Html->url(array("action" => "search"));?>"><input type="text" name="q" id="q" class="span3 search-query" ><button type="submit" class="btn">Search</button></form></div><?php }?>
            <h1>Steinlager Bars<?php if($steinlagerbars){?><br><small class="span12 offset5 pull-center"><?php echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));?></small><?php }?></h1>
        </div>
        <?php
        $this->Layout->sessionFlash();
        ?>
        <div class="row-fluid">
            <div class="span12 container-fluid">  
                <?php
                if( !empty($steinlagerbars) && sizeof($steinlagerbars)>0 ){
                ?>
                    <?php
                    echo str_replace('<form','<form class="form-horizontal"',$this->Form->create('SteinlagerBars', array(
                            'url' => array('controller' => 'steinlager_bars', 'action' => 'batch_delete')
                        ))
                    );
                    ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th style="width:20px;"><div><p><strong><input id="checkAll" type="checkbox" onclick="toggleCheckboxes();" /></strong></p></div></th>
                                <th style="width:200px;"><div><p><strong><?php echo $paginator->sort('Title','title');?></strong></p></div></th>
                                <th style="width:150px;"><div><p><strong><?php echo $paginator->sort('Address','address');?></strong></p></div></th>
                                <th style="width:150px;"><div><p><strong><?php echo $paginator->sort('Phone','phone');?></strong></p></div></th>
                                <th style="width:250px;"><div><p><strong>Details</strong></p></div></th>
                                <th style="width:100px;"><div><p><strong>Image</strong></p></div></th>
                                <th style="width:100px;"><div><p><strong>Pure Bar</strong></p></div></th>
                                <th style="width:100px;"><div><p><strong><?php echo $paginator->sort('Created At','created_at');?></strong></p></div></th>
                                <th style="width:100px;"><div><p><strong>Actions</strong></p></div></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach( $steinlagerbars as $steinlagerbar ){
                            
                                $thumbsrc = null;
                                $img_src = DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'original'.DIRECTORY_SEPARATOR.'steinlager_bars'.DIRECTORY_SEPARATOR.trim($steinlagerbar['SteinlagerBars']['image']);
                                $base_path = APP.'webroot'.$img_src;
                                if(is_file($base_path)){
                                    $thumbsrc = DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'thumbs'.DIRECTORY_SEPARATOR.'480_320'.DIRECTORY_SEPARATOR.'steinlager_bars'.DIRECTORY_SEPARATOR.'thumbs_'.trim($steinlagerbar['SteinlagerBars']['image']);
                                }
                            
                                $editUri = $this->Html->url(array("controller"=>"steinlager_bars","action"=>'edit',intval($steinlagerbar['SteinlagerBars']['id']).DIRECTORY_SEPARATOR.convertToSeoUri(trim($steinlagerbar['SteinlagerBars']['title']))));
                                $deleteUri = $this->Html->url(array("controller"=>"steinlager_bars","action"=>'delete',intval($steinlagerbar['SteinlagerBars']['id'])));
                            ?>
                            <tr>
                                <td><input type="checkbox" name="data[SteinlagerBars][id][]" value="<?php echo intval($steinlagerbar['SteinlagerBars']['id']);?>" /></td>
                                <td><div><p><strong><a href="<?php echo $editUri;?>"><?php echo trim($steinlagerbar['SteinlagerBars']['title']);?></a></strong></p></div></td>
                                <td><div><p><?php echo trim($steinlagerbar['SteinlagerBars']['address']);?></p></div></td>
                                <td><div><p><?php echo trim($steinlagerbar['SteinlagerBars']['phone']);?></p></div></td>
                                <td><div><p><?php echo $this->Text->truncate(trim($steinlagerbar['SteinlagerBars']['details']),150);?></p></div></td>
                                <td><div><p><?php if($thumbsrc){?><img src="<?php echo $thumbsrc;?>" height="45px" width="45px" /><?php }?></p></div></td>
                                <td><div><p><?php echo (intval($steinlagerbar['SteinlagerBars']['is_pure_bar'])==1)?'YES':'NO';?></p></div></td>
                                <td><div><p><?php echo date("D j",strtotime(trim($steinlagerbar['SteinlagerBars']['created_at']))).'<sup>'.date("S",strtotime(trim($steinlagerbar['SteinlagerBars']['created_at']))).'</sup>&nbsp;'.date("F",strtotime(trim($steinlagerbar['SteinlagerBars']['created_at'])));?></p></div></td>
                                <td style="text-align:right;">
                                    <div class="btn-group"><a class="btn" href="<?php echo $editUri;?>">Edit</a>    
                                    <a class="btn btn-danger" onclick="
                                        if (confirm('Are you sure?')) { 
                                            var f = document.createElement('form'); 
                                            f.style.display = 'none'; 
                                            this.parentNode.appendChild(f); 
                                            f.method = 'post'; 
                                            f.action = this.href;
                                            var m = document.createElement('input'); 
                                            m.setAttribute('type', 'hidden');
                                            m.setAttribute('name', 'sf_method'); 
                                            m.setAttribute('value', 'delete'); 
                                            f.appendChild(m);f.submit(); };return false;" href="<?php echo $deleteUri;?>">Delete</a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="10">
                                    <div class="pull-right">
                                        <div class="pagination">
                                            <ul>
                                                <?php
                                                //echo $paginator->first('<< First',array('model'=> 'CheapEats','tag'=>'li','class'=>'')).' '.
						//$paginator->prev('< Previous',array('model'=> 'CheapEats','tag'=>'li','class'=>'')).' '.
						echo $paginator->numbers(array('model'=> 'SteinlagerBars','tag'=>'li','class'=>'')).' ';
						//$paginator->next('Next >',array('model'=> 'CheapEats','tag'=>'li','class'=>'')).' '.
						//$paginator->last('Last >>',array('model'=> 'CheapEats','tag'=>'li','class'=>''));
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="btn-toolbar">
                                        <div class="btn-group"><a id="dropdown_toggle" class="btn dropdown-toggle" data-toggle="dropdown" href="#">Choose an action    <span class="caret"></span></a>
                                        <ul class="batch-actions dropdown-menu"><li><a href="#" data-action="batchDelete">Delete</a></li></ul></div>
                                        <input type="hidden" name="batch_action" value="" />
                                        <script type="text/javascript">
                                        $('.batch-actions a').bind('click', function(){
                                            var $anchor = $(this),
                                            $form = $anchor.closest('form');
                                            $('input[name="batch_action"]', $form).val($anchor.data('action'));
                                            $form.submit();
                                            return false;
                                        });
                                        
                                        $('#dropdown_toggle').dropdown();
                                        </script>
                                        <div class="btn-group"><a href="<?php echo $this->Html->url(array("controller" => "steinlager_bars","action" => "add"));?>" class="btn btn-info"><i class="icon-plus icon-white"></i> New</a></div>
                                    </div>
                                </th>
                            </tr>
                        </tfoot>
                    </table>  
                     <?php echo $this->Form->end();?>
                 </form>     
                <?php
                }else{
                ?>
                <p><a class="btn btn-primary btn-large" href="<?php echo $this->Html->url(array("controller" => "steinlager_bars","action" => "add"));?>" id="addtop50" role="button" data-toggle="modal">Add Steinlager Bar</a></p>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>