<?php echo $this->element('navigation');?>
<div class="clear:both;"></div><br><br>
<style type="text/css">
#map_canvas {
    background-color: #E5E3DF;
    height: 500px;
    overflow: hidden;
    position: relative;
    top: 0 !important;
}
</style>
<div class="container">
    <div class="row">
        <div class="span12">
            <div class="page-header"><h1>Edit "<?php echo $steinlagers['SteinlagerBars']['title'];?>"</h1></div>
            <?php
            $this->Layout->sessionFlash();
            $this->Html->addCrumb('Home', '/');
            $this->Html->addCrumb('Steinlager Bars', $this->Html->url(array("controller" => "steinlager_bars",'action'=>'index')));
            $this->Html->addCrumb($steinlagers['SteinlagerBars']['title']);
            echo $this->element('breadcrumb');
            ?>
            <div class="row-fluid">
                <div class="span8 container-fluid">  
                    <?php
                    echo str_replace(array('<form','PUT'),array('<form class="form-horizontal"','POST'),$this->Form->create('SteinlagerBars', array(
                            'url' => array('controller' => 'steinlager_bars', 'action' => 'edit',intval($steinlagers['SteinlagerBars']['id']).DIRECTORY_SEPARATOR.convertToSeoUri(strtolower(trim($steinlagers['SteinlagerBars']['title'])))),
                            'type' => 'file'
                        ))
                    );
                    ?>
                    <input type="hidden" name="data[SteinlagerBars][id]" id="id" value="<?php echo $steinlagers['SteinlagerBars']['id'];?>" />
                    <input type="hidden" name="data[SteinlagerBars][latitude]" id="latitude" value="<?php echo (!isset($post_steinlagers) && !isset($post_steinlagers['SteinlagerBars']['latitude']))? $steinlagers['SteinlagerBars']['latitude']:$post_steinlagers['SteinlagerBars']['latitude'];?>" />
                    <input type="hidden" name="data[SteinlagerBars][longitude]" id="longitude" value="<?php echo (!isset($post_steinlagers) && !isset($post_steinlagers['SteinlagerBars']['latitude']))? $steinlagers['SteinlagerBars']['longitude']:$post_steinlagers['SteinlagerBars']['longitude'];?>" />
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputTitle">Title</label>
                        <div class="controls"><input type="text" class="input-xlarge" id="inputTitle" name="data[SteinlagerBars][title]" value="<?php echo (!isset($post_steinlagers) && !isset($post_steinlagers['SteinlagerBars']['title']))? trim($steinlagers['SteinlagerBars']['title']):trim($post_steinlagers['SteinlagerBars']['title']);?>" maxlength="1000" placeholder="Title..." /></div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputAddress">Address</label>
                        <div class="controls"><input type="text" class="input-xlarge" id="inputAddress" name="data[SteinlagerBars][address]" value="<?php echo (!isset($post_steinlagers) && !isset($post_steinlagers['SteinlagerBars']['address']))? trim($steinlagers['SteinlagerBars']['address']):trim($post_steinlagers['SteinlagerBars']['address']);?>" maxlength="1000" placeholder="Address..." /><a class="btn" style="cursor:pointer" onclick="submitQuery();">Geocode</a></div>
                    </div>
                    <div class="control-group control-type-text" style="display:none;">
                        <label class="control-label" for="inputPhone">Phone Number</label>
                        <div class="controls"><input type="text" class="input-xlarge" id="inputPhone" name="data[SteinlagerBars][phone]" value="<?php echo (!isset($post_steinlagers) && !isset($post_steinlagers['SteinlagerBars']['phone']))? trim($steinlagers['SteinlagerBars']['phone']):trim($post_steinlagers['SteinlagerBars']['phone']);?>" maxlength="50" placeholder="Phone..." /></div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputInfo">Info</label>
                        <div class="controls"><textarea rows="5" class="input-xlarge" id="inputInfo" name="data[SteinlagerBars][details]" placeholder="Info text...."><?php echo (!isset($post_steinlagers) && !isset($post_steinlagers['SteinlagerBars']['details']))? trim($steinlagers['SteinlagerBars']['details']):trim($post_steinlagers['SteinlagerBars']['details']);?></textarea></div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputBar">Is Pure Bar</label>
                        <div class="controls">
                            <select class="input-xlarge" name="data[SteinlagerBars][is_pure_bar]" id="inputBar">
                                <option value="1"<?php echo (!isset($post_steinlagers) && !isset($post_steinlagers['SteinlagerBars']['phone']))? trim($steinlagers['SteinlagerBars']['phone']):trim($post_steinlagers['SteinlagerBars']['phone']);?>>Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputFacebook">Facebook Id</label>
                        <div class="controls"><input type="text" class="input-xlarge" id="inputFacebook" name="data[SteinlagerBars][facebook_id]" value="<?php echo (!isset($post_steinlagers) && !isset($post_steinlagers['SteinlagerBars']['facebook_id']))? trim($steinlagers['SteinlagerBars']['facebook_id']):trim($post_steinlagers['SteinlagerBars']['facebook_id']);?>" maxlength="50" placeholder="Facebook Id..." /></div>
                    </div>
                    <?php
                    $img_src = DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'original'.DIRECTORY_SEPARATOR.'steinlager_bars'.DIRECTORY_SEPARATOR.trim($steinlagers['SteinlagerBars']['image']);
                    $base_path = APP.'webroot'.$img_src;
                    if(is_file($base_path)){
                        $deleteUri = $this->Html->url(array("controller"=>"steinlager_bars","action"=>'delete_image',intval($steinlagers['SteinlagerBars']['id'])));
                    ?>
                    <div class="control-group offset3">
                        <a class="btn btn-danger" onclick="
                            if (confirm('Are you sure?')) { 
                                var f = document.createElement('form'); 
                                f.style.display = 'none'; 
                                this.parentNode.appendChild(f); 
                                f.method = 'post'; 
                                f.action = this.href;
                                var m = document.createElement('input'); 
                                m.setAttribute('type', 'hidden');
                                m.setAttribute('name', 'sf_method'); 
                                m.setAttribute('value', 'delete'); 
                                f.appendChild(m);f.submit(); };return false;" href="<?php echo $deleteUri;?>">Delete Image</a>
                    </div>
                    <div class="control-group">
                        <div class="controls"><img src="<?php echo $img_src?>" width="250" height="250" /></div>
                    </div>
                    <?php
                    }
                    ?>
                    <div class="control-group">
                        <label class="control-label" for="inputImage">Image</label>
                        <div class="controls"><input type="file" class="input-xlarge" id="inputImage" name="data[SteinlagerBars][image]" /></div>
                    </div>
                    <div class="form-actions"><div class="btn-group">
                        <button type="submit" class="btn btn-primary" id="inputSubmit" name="Login" placeholder="Save" value="Save">Save</button>&nbsp;&nbsp;<a class="btn" href="<?php echo $this->Html->url(array("controller"=>"steinlager_bars","action"=>'index'));?>">Back to list</a>
                    </div></div>
                    <?php echo $this->Form->end();?>
                </div>
                <div class="offset1">
                    <div id="map_canvas" style="height:500px;top:300px"></div>
                    <div style="display:none;">
                        <div id="responseInfo">
                            <div id="responseStatus">
                                <div>
                                <span style="font-weight: bold">Geocoder response: </span>
                                <span id="statusValue"></span> (<span id="statusDescription"></span>)
                                </div>
                            </div>
                            <div id="responseCount">
                                <span style="font-weight: bold;">Matches returned: </span>
                                <span id="matchCount"></span>
                            </div>
                        </div>
                        <div id="matches"></div>
                        <div id="boundsLegend">Bounds</div>
                        <div id="viewportLegend">Viewport</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br>
<script type="text/javascript">
/* <![CDATA[ */    
    $(document).ready(function(){
        is_edit = true;
        update_uri = '<?php echo $this->Html->url(array("controller"=>"steinlager_bars","action"=>'edit_geocode'));?>?';
        initialize();
        $("#inputPhone").mask("(09) 999-9999");
    });
/* ]]> */
</script>
