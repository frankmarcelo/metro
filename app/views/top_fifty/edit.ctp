<?php echo $this->element('navigation');?>
<div class="clear:both;"></div><br><br>
<style type="text/css">
#map_canvas {
    background-color: #E5E3DF;
    height: 500px;
    overflow: hidden;
    position: relative;
    top: 0 !important;
}
</style>
<div class="container">
    <div class="row">
        <div class="span12">
            <div class="page-header"><h1>Edit "<?php echo $toprestaurant['TopRestaurants']['title'];?>"</h1></div>
            <?php
            $this->Layout->sessionFlash();
            $this->Html->addCrumb('Home', '/');
            $this->Html->addCrumb('Top 50 Restaurants', $this->Html->url(array("controller" => "/",'action'=>'index')));
            $this->Html->addCrumb($toprestaurant['TopRestaurants']['title']);
            echo $this->element('breadcrumb');
            ?>
            <div class="row-fluid">
                <div class="span8 container-fluid">  
                    <?php
                    echo str_replace(array('<form','PUT'),array('<form class="form-horizontal"','POST'),$this->Form->create('TopRestaurants', array(
                            'url' => array('controller' => 'top_fifty', 'action' => 'edit',intval($toprestaurant['TopRestaurants']['id']).DIRECTORY_SEPARATOR.convertToSeoUri(strtolower(trim($toprestaurant['TopRestaurants']['title'])))),
                            'type' => 'file'
                        ))
                    );
                    ?>
                    <input type="hidden" name="data[TopRestaurants][id]" id="id" value="<?php echo $toprestaurant['TopRestaurants']['id'];?>" />
                    <input type="hidden" name="data[TopRestaurants][latitude]" id="latitude" value="<?php echo (!isset($post_toprestaurant) && !isset($post_toprestaurant['TopRestaurants']['latitude']))? $toprestaurant['TopRestaurants']['latitude']:$post_toprestaurant['TopRestaurants']['latitude'];?>" />
                    <input type="hidden" name="data[TopRestaurants][longitude]" id="longitude" value="<?php echo (!isset($post_toprestaurant) && !isset($post_toprestaurant['TopRestaurants']['latitude']))? $toprestaurant['TopRestaurants']['longitude']:$post_toprestaurant['TopRestaurants']['longitude'];?>" />
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputTitle">Title</label>
                        <div class="controls"><input type="text" class="input-xlarge" id="inputTitle" name="data[TopRestaurants][title]" value="<?php echo (!isset($post_toprestaurant) && !isset($post_toprestaurant['TopRestaurants']['title']))? trim($toprestaurant['TopRestaurants']['title']):trim($post_toprestaurant['TopRestaurants']['title']);?>" maxlength="1000" placeholder="Title..." /></div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputAddress">Address</label>
                        <div class="controls"><input type="text" class="input-xlarge" id="inputAddress" name="data[TopRestaurants][address]" value="<?php echo (!isset($post_toprestaurant) && !isset($post_toprestaurant['TopRestaurants']['address']))? trim($toprestaurant['TopRestaurants']['address']):trim($post_toprestaurant['TopRestaurants']['address']);?>" maxlength="1000" /><a class="btn" style="cursor:pointer" onclick="submitQuery();">Geocode</a></div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputPhone">Phone Number</label>
                        <div class="controls"><input type="text" class="input-xlarge" id="inputPhone" name="data[TopRestaurants][phone]" value="<?php echo (!isset($post_toprestaurant) && !isset($post_toprestaurant['TopRestaurants']['phone']))? trim($toprestaurant['TopRestaurants']['phone']):trim($post_toprestaurant['TopRestaurants']['phone']);?>" maxlength="50" placeholder="Phone..." /></div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputInfo">Info</label>
                        <div class="controls"><textarea rows="5" class="input-xlarge" id="inputInfo" name="data[TopRestaurants][details]" placeholder="Info text...."><?php echo (!isset($post_toprestaurant) && !isset($post_toprestaurant['TopRestaurants']['details']))? trim($toprestaurant['TopRestaurants']['details']):trim($post_toprestaurant['TopRestaurants']['details']);?></textarea></div>
                    </div>
                    <?php
                    
                    $img_src = DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'original'.DIRECTORY_SEPARATOR.'top_restaurants'.DIRECTORY_SEPARATOR.trim($toprestaurant['TopRestaurants']['image']);
                    $base_path = APP.'webroot'.$img_src;
                    if(is_file($base_path)){
                    ?>
                    <div class="control-group">
                        <div class="controls"><img src="<?php echo $img_src?>" width="250" height="250" /></div>
                    </div>
                    <?php
                    }
                    ?>
                    <div class="control-group">
                        <label class="control-label" for="inputImage">Image</label>
                        <div class="controls"><input type="file" class="input-xlarge" id="inputImage" name="data[TopRestaurants][image]" /></div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputQualityRating">Quality Rating</label>
                        <div class="controls">
                            <select class="input-xlarge" name="data[TopRestaurants][quality_rating]" id="inputQualityRating">
                            <?php 
                            for( $i = 1; $i < 6; $i++ ){
                                if(!isset($post_toprestaurant) && !isset($post_toprestaurant['TopRestaurants']['quality_rating'])){
                            ?>
                            <option value="<?php echo $i;?>"<?php echo ($i==intval($toprestaurant['TopRestaurants']['quality_rating']))?' selected="selected"':'';?>><?php echo $i;?></option>
                            <?php
                                }else{
                            ?>
                            <option value="<?php echo $i;?>"<?php echo ($i==intval($post_toprestaurant['TopRestaurants']['quality_rating']))?' selected="selected"':'';?>><?php echo $i;?></option>
                            <?php
                                }
                            }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputPriceRating">Price Rating</label>
                        <div class="controls">
                            <select class="input-xlarge" name="data[TopRestaurants][price_rating]" id="inputPriceRating">
                            <?php 
                            for( $i = 1; $i < 6; $i++ ){
                                if(!isset($post_toprestaurant) && !isset($post_toprestaurant['TopRestaurants']['quality_rating'])){
                            ?>
                            <option value="<?php echo $i;?>"<?php echo ($i==intval($toprestaurant['TopRestaurants']['price_rating']))?' selected="selected"':'';?>><?php echo $i;?></option>
                            <?php
                                }else{
                            ?>
                            <option value="<?php echo $i;?>"<?php echo ($i==intval($post_toprestaurant['TopRestaurants']['price_rating']))?' selected="selected"':'';?>><?php echo $i;?></option>
                            <?php
                                }
                            }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputFavouriteDish">Favourite Dish</label>
                        <div class="controls"><textarea rows="5" class="input-xlarge" id="inputFavouriteDish" name="data[TopRestaurants][favourite_dish]" placeholder="Favourite Dish...."><?php echo (!isset($post_toprestaurant) && !isset($post_toprestaurant['TopRestaurants']['favourite_dish']))? trim($toprestaurant['TopRestaurants']['favourite_dish']):trim($post_toprestaurant['TopRestaurants']['favourite_dish']);?></textarea></div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputOpeningHours">Opening Hours</label>
                        <div class="controls"><textarea rows="5" class="input-xlarge" id="inputOpeningHours" name="data[TopRestaurants][opening_hours]" placeholder="Opening hours...."><?php echo (!isset($post_toprestaurant) && !isset($post_toprestaurant['TopRestaurants']['opening_hours']))? trim($toprestaurant['TopRestaurants']['opening_hours']):trim($post_toprestaurant['TopRestaurants']['opening_hours']);?></textarea></div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputFacebook">Facebook Id</label>
                        <div class="controls"><input type="text" class="input-xlarge" id="inputFacebook" name="data[TopRestaurants][facebook_id]" value="<?php echo (!isset($post_toprestaurant) && !isset($post_toprestaurant['TopRestaurants']['facebook_id']))? trim($toprestaurant['TopRestaurants']['facebook_id']):trim($post_toprestaurant['TopRestaurants']['facebook_id']);?>" maxlength="50" placeholder="Facebook Id..." /></div>
                    </div>
                    <div class="form-actions"><div class="btn-group">
                        <button type="submit" class="btn btn-primary" id="inputSubmit" name="Login" placeholder="Save" value="Save">Save</button>&nbsp;&nbsp;<a class="btn" href="<?php echo str_replace("top_fifty","",$this->Html->url(array("controller"=>"top_fifty","action"=>'index')));?>">Back to list</a>
                    </div></div>
                    <?php echo $this->Form->end();?>
                </div>
                <div class="offset1">
                    <div id="map_canvas" style="height:500px;top:300px"></div>
                    <div style="display:none;">
                        <div id="responseInfo">
                            <div id="responseStatus">
                                <div>
                                <span style="font-weight: bold">Geocoder response: </span>
                                <span id="statusValue"></span> (<span id="statusDescription"></span>)
                                </div>
                            </div>
                            <div id="responseCount">
                                <span style="font-weight: bold;">Matches returned: </span>
                                <span id="matchCount"></span>
                            </div>
                        </div>
                        <div id="matches"></div>
                        <div id="boundsLegend">Bounds</div>
                        <div id="viewportLegend">Viewport</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br>
<script type="text/javascript">
/* <![CDATA[ */    
    $(document).ready(function(){
        is_edit = true;
        update_uri = '<?php echo $this->Html->url(array("controller"=>"top_fifty","action"=>'edit_geocode'));?>?';
        initialize(); 
        $("#inputPhone").mask("(09) 999-9999");
    });
/* ]]> */
</script>
