<?php echo $this->element('navigation');?>
<div class="clear:both;"></div><br><br>
<style type="text/css">
#map_canvas {
    background-color: #E5E3DF;
    height: 500px;
    overflow: hidden;
    position: relative;
    top: 0 !important;
}
</style>
<div class="container">
    <div class="row">
        <div class="span12">
            <div class="page-header"><h1>Add Top 50 Restaurants</h1></div>
            <?php
            $this->Layout->sessionFlash();
            
            $this->Html->addCrumb('Home', '/');
            $this->Html->addCrumb('Top Restaurants', '/');
            $this->Html->addCrumb('Add');
            echo $this->element('breadcrumb');
            ?>
            <div class="row-fluid">
                <div class="span8 container-fluid">  
                    <?php
                    echo str_replace('<form','<form class="form-horizontal"',$this->Form->create('TopRestaurants', array(
                            'url' => array('controller' => 'top_fifty', 'action' => 'add'),
                            'type' => 'file'
                        ))
                    );
                    ?>
                    <input type="hidden" name="data[TopRestaurants][latitude]" id="latitude" value="<?php echo (!isset($toprestaurant) && !isset($toprestaurant['TopRestaurants']['latitude']))? Configure::read('GoogleGeocode.Latitude'):$toprestaurant['TopRestaurants']['latitude'];?>" />
                    <input type="hidden" name="data[TopRestaurants][longitude]" id="longitude" value="<?php echo (!isset($toprestaurant) && !isset($toprestaurant['TopRestaurants']['longitude']))? Configure::read('GoogleGeocode.Longitude'):$toprestaurant['TopRestaurants']['longitude'];?>" />
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputTitle">Title</label>
                        <div class="controls"><input type="text" class="input-xlarge" id="inputTitle" value="<?php echo (!isset($toprestaurant) && !isset($toprestaurant['TopRestaurants']['title']))? '':trim($toprestaurant['TopRestaurants']['title']);?>" name="data[TopRestaurants][title]" maxlength="100" placeholder="Title..." /></div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputAddress">Address</label>
                        <div class="controls"><input type="text" class="input-xlarge" id="inputAddress" value="<?php echo (!isset($toprestaurant) && !isset($toprestaurant['TopRestaurants']['address']))? '':trim($toprestaurant['TopRestaurants']['address']);?>" name="data[TopRestaurants][address]" maxlength="100" placeholder="Address..." /><a class="btn" style="cursor:pointer" onclick="submitQuery()">Geocode</a></div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputPhone">Phone Number</label>
                        <div class="controls"><input type="text" class="input-xlarge" id="inputPhone" value="<?php echo (!isset($toprestaurant) && !isset($toprestaurant['TopRestaurants']['phone']))? '':trim($toprestaurant['TopRestaurants']['phone']);?>" name="data[TopRestaurants][phone]" maxlength="50" placeholder="Phone..." /></div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputInfo">Info</label>
                        <div class="controls"><textarea rows="5" class="input-xlarge" id="inputInfo" name="data[TopRestaurants][details]" placeholder="Info text...."><?php echo (!isset($toprestaurant) && !isset($toprestaurant['TopRestaurants']['details']))? '':trim($toprestaurant['TopRestaurants']['details']);?></textarea></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputImage">Image</label>
                        <div class="controls"><input type="file" class="input-xlarge" id="inputImage" name="data[TopRestaurants][image]" /></div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputQualityRating">Quality Rating</label>
                        <div class="controls">
                            <select class="input-xlarge" name="data[TopRestaurants][quality_rating]" id="inputQualityRating">
                            <?php 
                            for( $i = 1; $i < 6; $i++ ){
                            ?>
                            <option value="<?php echo $i;?>"<?php echo ($i==1 || (isset($toprestaurant) && isset($toprestaurant['TopRestaurants']['quality_rating']) && $toprestaurant['TopRestaurants']['quality_rating'] == $i ) )?' selected="selected"':'';?>><?php echo $i;?></option>
                            <?php
                            }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputPriceRating">Price Rating</label>
                        <div class="controls">
                            <select class="input-xlarge" name="data[TopRestaurants][price_rating]" id="inputPriceRating">
                            <?php 
                            for( $i = 1; $i < 6; $i++ ){
                            ?>
                            <option value="<?php echo $i;?>"<?php echo ($i==1 || (isset($toprestaurant) && isset($toprestaurant['TopRestaurants']['price_rating']) && $toprestaurant['TopRestaurants']['price_rating'] == $i ) )?' selected="selected"':'';?>><?php echo $i;?></option>
                            <?php
                            }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputFavouriteDish">Favourite Dish</label>
                        <div class="controls"><textarea rows="5" class="input-xlarge" id="inputFavouriteDish" name="data[TopRestaurants][favourite_dish]" placeholder="Favourite Dish...."><?php echo (!isset($toprestaurant) && !isset($toprestaurant['TopRestaurants']['favourite_dish']))? '':trim($toprestaurant['TopRestaurants']['favourite_dish']);?></textarea></div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputOpeningHours">Opening Hours</label>
                        <div class="controls"><textarea rows="5" class="input-xlarge" id="inputOpeningHours" name="data[TopRestaurants][opening_hours]" placeholder="Opening hours...."><?php echo (!isset($toprestaurant) && !isset($toprestaurant['TopRestaurants']['opening_hours']))? '':trim($toprestaurant['TopRestaurants']['opening_hours']);?></textarea></div>
                    </div>
                    <div class="control-group control-type-text">
                        <label class="control-label" for="inputFacebook">Facebook Id</label>
                        <div class="controls"><input type="text" class="input-xlarge" id="inputFacebook" name="data[TopRestaurants][facebook_id]" value="<?php echo (!isset($toprestaurant) && !isset($toprestaurant['TopRestaurants']['facebook_id']))? '':trim($toprestaurant['TopRestaurants']['facebook_id']);?>" maxlength="50" placeholder="Facebook Id..." /></div>
                    </div>
                    <div class="form-actions"><div class="btn-group">
                        <button type="submit" class="btn btn-primary" id="inputSubmit" name="Login" placeholder="Save" value="Save">Save</button>&nbsp;&nbsp;<a class="btn" href="<?php echo str_replace("top_fifty","",$this->Html->url(array("controller"=>"top_fifty","action"=>'index')));?>">Back to list</a>
                    </div></div>
                    <?php echo $this->Form->end();?>
                </div>
                <div class="offset1">
                    <div id="map_canvas" style="height:500px;top:300px"></div>
                    <div style="display:none;">
                        <div id="responseInfo">
                            <div id="responseStatus">
                                <div>
                                <span style="font-weight: bold">Geocoder response: </span>
                                <span id="statusValue"></span> (<span id="statusDescription"></span>)
                                </div>
                            </div>
                            <div id="responseCount">
                                <span style="font-weight: bold;">Matches returned: </span>
                                <span id="matchCount"></span>
                            </div>
                        </div>
                        <div id="matches"></div>
                        <div id="boundsLegend">Bounds</div>
                        <div id="viewportLegend">Viewport</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br>
<script type="text/javascript">
/* <![CDATA[ */    
    $(document).ready(function(){
        initialize(); 
        $("#inputPhone").mask("(09) 999-9999");
    });
/* ]]> */
</script>
