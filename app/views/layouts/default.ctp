<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en" xmlns:fb="https://www.facebook.com/2008/fbml"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en" xmlns:fb="https://www.facebook.com/2008/fbml"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en" xmlns:fb="https://www.facebook.com/2008/fbml"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" xmlns:fb="https://www.facebook.com/2008/fbml"> <!--<![endif]-->
<head>
    <meta property="og:title" content="Metro Mobile API" />
    <meta property="og:type" content="landmark" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="Metro Mobile API" />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><?php echo $title_for_layout; ?></title>
    <meta name="description" content="<?php //echo $meta_descriptions;?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
    <link rel="shortcut icon" href="/favicon.ico" />
    <link rel="apple-touch-icon" href="/img/apple-touch-icon-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/img/apple-touch-icon-114x114-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/img/apple-touch-icon-72x72-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/img/apple-touch-icon-57x57-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" href="/img/apple-touch-icon-precomposed.png" />
    <link rel="author" href="/humans.txt" />
    <?php 
    echo $this->Html->css(array('bootstrap.min','/min/?g=css'));
    echo $this->Html->script(array('libs/modernizr'));
    ?>
    <style type="text/css">
    .input-xlarge {
        width: 420px !important;
    }
    </style>
    <link type="text/css" href="/css/ui-lightness/ui.css" rel="stylesheet" />
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3.5&amp;sensor=true&amp;region=NZ"></script>
    <?php echo $this->element('misc_js');?>
</head>
<body>
    <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
    <![endif]-->
    <div class="container-fluid"><?php echo $content_for_layout;?></div>
    <?php echo $this->Html->script(array('bootstrap/bootstrap.min'));?>
</body>
</html>