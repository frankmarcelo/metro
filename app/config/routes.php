<?php

Router::mapResources(array('webservices'));
Router::parseExtensions('rss','xml','json');
Router::connect('/', array('controller' => 'top_fifty', 'action' => 'index'));
Router::connect('/search', array('controller' => 'top_fifty', 'action' => 'search'));
Router::connect('/top-50', array('controller' => 'top_fifty', 'action' => 'index', 1));
Router::connect('/cheap-eats', array('controller' => 'cheap_eats', 'action' => 'index', 1));
Router::connect('/steinlager-bars', array('controller' => 'steinlager_bars', 'action' => 'index', 1));
Router::connect('/webservices/toprestaurants.json', array('controller' => 'webservices', 'action' => 'TopRestaurants'));
Router::connect('/webservices/cheapeats.json', array('controller' => 'webservices', 'action' => 'CheapEats'));
Router::connect('/webservices/steinlagerbars.json', array('controller' => 'webservices', 'action' => 'SteinlagerBars'));
