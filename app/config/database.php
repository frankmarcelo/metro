<?php

class DATABASE_CONFIG {

	var $default = array(
		'driver' => 'mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'fr4nkl1n',
		'database' => 'metro',
		'prefix' => '',
		'encoding' => 'utf8'
	);

	var $test = array(
		'driver' => 'mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'Password123',
		'database' => 'metro',
		'prefix' => '',
		'encoding' => 'utf8'
	);
}
