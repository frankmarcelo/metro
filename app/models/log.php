<?php
/**
 * User
 *
 * PHP version 5
 *
 */
class Log extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'Log';
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $order = 'id ASC';
    public $cacheQueries = true;
}