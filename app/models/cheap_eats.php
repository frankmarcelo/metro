<?php

class CheapEats extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'CheapEats';
    public $cacheQueries = true;
    public $useTable = 'cheap_eats';
    public $actsAs   = array('CacheQueries','Transactional','Containable');
      
    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => true,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}