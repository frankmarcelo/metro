<?php

class SteinlagerBars extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'SteinlagerBars';
    public $cacheQueries = true;
    public $useTable = 'steinlager_bars';
    public $actsAs   = array('CacheQueries','Transactional','Containable');
      
    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => true,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}